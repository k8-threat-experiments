/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif

#include <alloca.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <SDL.h>

#include "dbglog/dbglog.h"
#include "videolib/videolib.h"
#include "libios/ios.h"

#include "fileio.h"

#include "common.h"
#include "zatan2i.h"
#include "zpolymod.h"
#include "zlight.h"

#include "tlevel.h"
#include "tgfx.h"


////////////////////////////////////////////////////////////////////////////////
#include "theo_contour.c"
#include "rdp.c"


////////////////////////////////////////////////////////////////////////////////
enum {
  PL_KEY_LEFT,
  PL_KEY_RIGHT,
  PL_KEY_UP,
  PL_KEY_DOWN,
  PL_KEY_FIRE
};

typedef struct {
  int x, y; /* player center */
  int dir; /* direction: 0==up; clockwise */
  int walkanim; /* 0,1,2,1 */
  int shooting;
  int dead;
  int keys[5];
} player_t;


static player_t player[3];
static int monster_lit_flag[255];


static void prepare_loaded_level (void) {
  reset_level_lights();
  /* setup players */
  for (int f = 0; f < 3; ++f) {
    player[f].x = plrpos[f].x;
    player[f].y = plrpos[f].y;
    player[f].dir = 0;
    player[f].walkanim = 0;
    player[f].dead = 0;
    memset(player[f].keys, 0, sizeof(player[f].keys));
  }
  memset(monster_lit_flag, 0, sizeof(monster_lit_flag));
}


static void monster_hit_by_light (int mnum, int x, int y, int alpha) {
  monster_lit_flag[mnum] = 1;
}


////////////////////////////////////////////////////////////////////////////////
static char *level_file_name = NULL;
static int episode = 1;
static int mission = 1;


////////////////////////////////////////////////////////////////////////////////
static int ms_map_x = -1, ms_map_y = -1;
static int hilight_passages = 0; /* bit0: passable; bit1: impassable */
static int show_palette = 0;
static int show_orders = 0;

static int light_x = 320/2, light_y = 400/2;
static int light_radius = 150;
static Uint32 light_color = /*rgba2col(255, 127, 0, 200)*/0x80ffffffU;
static int light_as = 0, light_ae = 0;
static int no_lights = 0;

////////////////////////////////////////////////////////////////////////////////
static theo_contour_data_t tcd;
static int contouring = 0;
static Uint32 next_contour_step_time;

static int contour_no_optimize = 0;
static point_t *contour = NULL;
static int contour_count = 0;
static int contour_alloted = 0;

static point_t *ctr_simple = NULL;
static int ctr_simple_count = 0;
static int ctr_simple_alloted = 0;


static int is_pix_filled (int x, int y) {
  return (map_get_pixel_orig(x, y) >= 16);
}


static void add_pix_no_optimize (int x, int y) {
  if (contour_count+1 > contour_alloted) {
    contour_alloted = ((contour_count+1)|0x1fff)+1;
    contour = realloc(contour, contour_alloted*sizeof(contour[0]));
  }
  contour[contour_count].x = x;
  contour[contour_count].y = y;
  ++contour_count;
}


static void add_pix (int x, int y) {
  //mapbmp_orig[y][x] = 48;
  if (!contour_no_optimize && contour_count > 2) {
    /* try to optimize line segments */
    int seg_sx = contour[contour_count-2].x;
    int seg_sy = contour[contour_count-2].y;
    int seg_ex = contour[contour_count-1].x;
    int seg_ey = contour[contour_count-1].y;
    if (seg_sy == seg_ey && y == seg_ey && K8SIGN(seg_sx-seg_ex) == K8SIGN(seg_sx-x)) {
      /* extend horizontal segment */
      contour[contour_count-1].x = x;
      return;
    } else if (seg_sx == seg_ex && x == seg_ex && K8SIGN(seg_sy-seg_ey) == K8SIGN(seg_sy-y)) {
      /* extend vertical segment */
      contour[contour_count-1].y = y;
      return;
    } else if (K8ABS(seg_sy-seg_ey) == K8ABS(seg_sx-seg_ex) &&
               K8ABS(seg_sy-y) == K8ABS(seg_sx-x) &&
               K8SIGN(seg_sy-seg_ey) == K8SIGN(seg_sy-y) &&
               K8SIGN(seg_sx-seg_ex) == K8SIGN(seg_sx-x)) {
      /* diagonal segment */
      contour[contour_count-1].x = x;
      contour[contour_count-1].y = y;
      return;
    }
  }
  add_pix_no_optimize(x, y);
}


static void contour_draw (const point_t *ctr, int count, int do_last, Uint32 clr) {
  if (ctr != NULL && count > 2) {
    for (int f = 0; f < count-1; ++f) draw_line_no_last(ctr[f].x, ctr[f].y, ctr[f+1].x, ctr[f+1].y, clr);
    if (do_last) draw_line_no_last(ctr[count-1].x, ctr[count-1].y, ctr[0].x, ctr[0].y, clr);
  }
}


////////////////////////////////////////////////////////////////////////////////
#include "coldet.c"


////////////////////////////////////////////////////////////////////////////////
static const int dir_delta[8][2] = {
  { 0, -1},
  { 1, -1},
  { 1,  0},
  { 1,  1},
  { 0,  1},
  {-1,  1},
  {-1,  0},
  {-1, -1}
};


static inline int player_check_collision (int x, int y) {
  return check_ellipse_collision(x+TILE_WIDTH/2-3-5, y+TILE_HEIGHT/2-5, x+TILE_WIDTH/2-3+5, y+TILE_HEIGHT/2+2+5);
}


/* try to move; return # of steps done; dir: 0-31 */
static int player_slide_move (player_t *pl, int dir, int dist) {
  int moved = 0;
  if (dist < 0) { dir += 16; dist = -dist; } /* turn around */
  dir = (((dir%32)+32)%32)/4; /* convert to normal dir */
  while (dist > 0) {
    int done;
    int max_strafe = (dist > 4 ? 4 : dist); /* strafe threshold */
    int dx = dir_delta[dir][0];
    int dy = dir_delta[dir][1];
    int nx = pl->x+dx;
    int ny = pl->y+dy;
    if (!player_check_collision(nx, ny)) { pl->x = nx; pl->y = ny; --dist; ++moved; continue; }
    /* can't move forward, try diagonal moves */
    nx = pl->x+dir_delta[(dir+7)&7][0];
    ny = pl->y+dir_delta[(dir+7)&7][1];
    if (!player_check_collision(nx, ny)) { pl->x = nx; pl->y = ny; --dist; ++moved; continue; }
    nx = pl->x+dir_delta[(dir+1)&7][0];
    ny = pl->y+dir_delta[(dir+1)&7][1];
    if (!player_check_collision(nx, ny)) { pl->x = nx; pl->y = ny; --dist; ++moved; continue; }
    /* check if we can stafe up to max_strafe pixels and THEN move forward */
    done = 0;
    for (int dd = 0; dd <= 1 && !done; ++dd) {
      int sdx = dir_delta[(dir+6+dd*4)&7][0];
      int sdy = dir_delta[(dir+6+dd*4)&7][1];
      nx = pl->x;
      ny = pl->y;
      for (int f = 1; f <= max_strafe; ++f) {
        nx += sdx;
        ny += sdy;
        if (player_check_collision(nx, ny)) break; /* can't strafe */
        if (!player_check_collision(nx+dx, ny+dy)) {
          /* we can strafe and then go forward, do strafing */
          /* don't do unnecessary work, move by 'f' pixels */
          pl->x = nx;
          pl->y = ny;
          dist -= f;
          moved += f;
          done = 1;
          break;
        }
      }
    }
    if (!done) break;
  }
  return moved;
}


static void process_player_keys (int plnum) {
  player_t *pl = &player[plnum];
  if (!pl->dead) {
    int mstep = 0;
    /* turning */
    if (pl->keys[PL_KEY_LEFT]) pl->dir = (pl->dir+31)%32;
    if (pl->keys[PL_KEY_RIGHT]) pl->dir = (pl->dir+1)%32;
    /* moving */
    // y: 6,3
    if (pl->keys[PL_KEY_UP]) mstep = 4;
    if (pl->keys[PL_KEY_DOWN]) mstep = -2;
    /* try to move */
    if (player_slide_move(pl, pl->dir, mstep)) {
      pl->walkanim = (pl->walkanim+(mstep > 0 ? 1 : 15))%16;
    } else {
      pl->walkanim = 0;
    }
    pl->shooting = (pl->keys[PL_KEY_FIRE] != 0);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void draw_palette (void) {
  fill_rect(0, 0, 320, 320, 0);
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < 16; ++x) {
      fill_rect(x*20, y*20, 18, 18, palette[y*16+x]);
    }
  }
}


static void rebuild_screen (void) {
  //fill_rect(0, 0, SCR_WIDTH, SCR_HEIGHT, rgb2col(0, 0, 0));
  //memset(sdl_vscr, 0, sizeof(sdl_vscr));
  Uint32 stt, rle, dre;
  /* restore map bitmap */
  process_player_keys(0);
  /* contouring */
  if (contouring) {
    if (SDL_GetTicks() >= next_contour_step_time) {
      next_contour_step_time = SDL_GetTicks()+0;
      contouring = theo_contour_step(&tcd);
    }
  }
  /* */
  fill_rect(0, MAP_RES_Y, SCR_WIDTH, SCR_HEIGHT-MAP_RES_Y, 0);
  memcpy(mapbmp, mapbmp_orig, sizeof(mapbmp));
  memset(monbmp, 0, sizeof(monbmp)); /* clear monster bitmap */
  /* draw monsters on monster bitmap */
  memset(monster_lit_flag, 0, sizeof(monster_lit_flag));
  for (int f = 0; f < mwaves[0].count; ++f) {
    monster_info_t *m = &mwaves[0].m[f];
    tr_draw_monster_on_mon(m->pos.x+2, m->pos.y-1, mainms[m->type].sprno, f+1);
  }
  /* light map */
  stt = SDL_GetTicks();
  if (!no_lights) {
    lb_reset();
    /* render level lights */
    render_lights();
    /* sample light */
    //light_dump = 1;
    for (int tt = 20*0+1; tt > 0; --tt) {
      renderlight_sector(light_x, light_y, light_radius, light_as, light_ae, light_color);
    }
    //light_dump = 0;
    /* player lights and warfog spheres */
    for (int f = 0; f < 3; ++f) {
      if (!player[f].dead) {
        int fangle = (360*player[f].dir/32)-90; /* 'forward' angle */
        fangle = ((fangle%360)+360)%360;
        player_light = 1;
        renderlight_sector(player[f].x+TILE_WIDTH/2, player[f].y+TILE_HEIGHT/2, 102, fangle-20, fangle+20, rgba2col(255, 255, 0, 160));
        player_light = 0;
        render_warfog_sphere(player[f].x+TILE_WIDTH/2, player[f].y+TILE_HEIGHT/2, 80);
      }
    }
    rle = SDL_GetTicks();
    /* draw visible monsters on map bitmap */
    for (int f = 0; f < mwaves[0].count; ++f) {
      const monster_info_t *m = &mwaves[0].m[f];
      if (monster_lit_flag[f]) tr_draw_monster_on_map(m->pos.x+2, m->pos.y-1, mainms[m->type].sprno);
    }
    /* render lit map */
    lb_draw();
    dre = SDL_GetTicks();
  } else {
    rle = dre = stt;
    /* draw visible monsters on map bitmap */
    for (int f = 0; f < mwaves[0].count; ++f) {
      const monster_info_t *m = &mwaves[0].m[f];
      tr_draw_monster_on_map(m->pos.x+2, m->pos.y-1, mainms[m->type].sprno);
    }
    const Uint8 *src = (void *)mapbmp;
    Uint32 *dest = sdl_vscr;
    for (size_t f = 0; f < MAP_RES_X*MAP_RES_Y; ++f) *dest++ = palette[*src++];
  }
  /* */
  if (hilight_passages) {
    const Uint8 *src = (void *)mapbmp;
    for (size_t f = 0; f < MAP_RES_X*MAP_RES_Y; ++f, ++src) {
      if (f >= MAP_RES_Y*320) break;
      if ((hilight_passages&0x01) && src[0] < 16) {
        put_pixel(f%320, f/320, rgba2col(0, 255, 0, 180));
      }
      if ((hilight_passages&0x02) && src[0] >= 16) {
        put_pixel(f%320, f/320, rgba2col(255, 0, 0, 160));
      }
    }
  }
  /* draw players */
  for (int f = 2; f >= 0; --f) {
    if (!player[f].dead) {
      int sn = 32*f;
      /* head center */
      int px = player[f].x;
      int py = player[f].y;
      if (player[f].shooting) sn += 3; else sn +=  player[f].walkanim/4-(player[f].walkanim/4 == 3 ? 2 : 0);
      tr_draw_otherspr(px, py, sn+(player[f].dir/4*4));
    }
  }
  if (contour_count > 2) {
    char buf[16];
    contour_draw(contour, contour_count, !contouring, rgba2col(255, 0, 0, (contouring ? 110 : 60)));
    snprintf(buf, sizeof(buf), "cc=%d", contour_count);
    for (int dy = -1; dy <= 1; ++dy) for (int dx = -1; dx <= 1; ++dx) if (dx | dy) tr_draw_str(1+dx, 1+dy, buf, rgb2col(0, 0, 0));
    tr_draw_str(1, 1, buf, rgb2col(0, 255, 0));
    if (!contouring && contour_count > 3) {
      /* simplify */
      /* explicitly add last pixel */
      add_pix_no_optimize(contour[0].x, contour[0].y);
      /* allocate dest array */
      if (ctr_simple_alloted < contour_count) {
        ctr_simple = realloc(ctr_simple, contour_count*sizeof(ctr_simple[0]));
        ctr_simple_alloted = contour_count;
      }
      ctr_simple_count = rdp_simplify(contour, contour_count, ctr_simple, 1.5f);
      if (ctr_simple_count > 3) {
        //static int shown = 0;
        --ctr_simple_count;
        snprintf(buf, sizeof(buf), "cs=%d", ctr_simple_count);
        for (int dy = -1; dy <= 1; ++dy) for (int dx = -1; dx <= 1; ++dx) if (dx | dy) tr_draw_str(1+dx, 12+dy, buf, rgb2col(0, 0, 0));
        tr_draw_str(1, 12, buf, rgb2col(0, 255, 0));
        contour_draw(ctr_simple, ctr_simple_count, 1, rgba2col(0, 255, 0, 90));
        /*
        if (!shown) {
          shown = 1;
          for (int f = 0; f < ctr_simple_count; ++f) fprintf(stderr, "%3d: (%d,%d)\n", f, ctr_simple[f].x, ctr_simple[f].y);
        }
        */
      }
      --contour_count; /* remove last pixel */
    }
  }
  /* draw level title */
  if (level_file_name == NULL) {
    char buf[64];
    snprintf(buf, sizeof(buf), "E%dM%d: %s", episode, mission, lvltitle);
    tr_draw_str(0, 400-8, buf, rgb2col(255, 127, 0));
  } else {
    tr_draw_str(0, 400-8, lvltitle, rgb2col(255, 127, 0));
  }
  /* draw orders data */
  {
    if (orders[ORDER_DESTROY_ALL]) {
      for (size_t f = 0; f < sizeof(explos)/sizeof(explos[0]); ++f) {
        if (explos[f].size > 0) {
          draw_rect(explos[f].pos.x, explos[f].pos.y, explos[f].size, explos[f].size, rgb2col(255, 0, 0));
        }
      }
    }
    if (orders[ORDER_BE_HERE]) {
      draw_rect(behere_pos.x, behere_pos.y, TILE_WIDTH*2, TILE_HEIGHT*2, rgb2col(0, 0, 255));
    }
  }
  /* show orders */
  if (show_orders) {
    static const char *hdr = "ORDERS";
    static const char *no_text = "NO ORDERS GIVEN";
    int order_alpha = 150;
    int order_text_alpha = 110;
    int cnt = 2, maxw = strlen(hdr), no_orders;
    for (int f = 0; f < 4; ++f) {
      if (orders[f]) {
        ++cnt;
        if (strlen(order_text[f]) > maxw) maxw = strlen(order_text[f]);
      }
    }
    if (cnt == 2) {
      ++cnt;
      maxw = strlen(no_text);
      no_orders = 1;
    } else {
      no_orders = 0;
    }
    draw_rect(2, 2, maxw*8+4, cnt*8+4, rgba2col(255, 255, 255, order_alpha));
    fill_rect(3, 3, maxw*8+2, cnt*8+2, rgba2col(0, 0, 128, order_alpha));
    draw_hline(4, 4+8+3, maxw*8, rgba2col(255, 255, 255, order_alpha));
    tr_draw_str(4+(maxw*8+2-strlen(hdr)*8)/2, 5, hdr, rgba2col(255, 255, 255, order_text_alpha));
    cnt = 4+2*8;
    if (no_orders) {
      tr_draw_str(4, cnt, no_text, rgba2col(255, 0, 0, order_text_alpha));
    } else {
      for (int f = 0; f < 4; ++f) {
        if (orders[f]) {
          tr_draw_str(4, cnt, order_text[f], rgba2col(255, 255, 0, order_text_alpha));
          cnt += 8;
        }
      }
    }
  }
  /* palette mode? */
  if (show_palette) {
    draw_palette();
    if (ms_map_x >= 0 && ms_map_y >= 0 && ms_map_x < 320 && ms_map_y < 320 && ms_map_x%20 < 18 && ms_map_y%20 < 18) {
      char buf[256];
      int cnum = (ms_map_y/20)*16+(ms_map_x/20);
      snprintf(buf, sizeof(buf), "%3d (r:%03u; g:%03u; b:%03u", cnum, (palette[cnum]>>16)&0xff, (palette[cnum]>>8)&0xff, (palette[cnum])&0xff);
      tr_draw_str(32, 400-16, buf, rgb2col(255, 127, 0));
      fill_rect(0, 400-16, 30, 8, palette[cnum]);
    }
  } else {
    /* draw pixel index under the mouse */
    if (ms_map_x >= 0 && ms_map_y >= 0 && ms_map_x < MAP_RES_X && ms_map_y < MAP_RES_Y) {
      char buf[256];
      snprintf(buf, sizeof(buf), "%3d x=%2d y=%2d tile=%u", mapbmp[ms_map_y][ms_map_x], ms_map_x/TILE_WIDTH, ms_map_y/TILE_HEIGHT, map[ms_map_y/TILE_HEIGHT][ms_map_x/TILE_WIDTH]);
      tr_draw_str(32, 400-16, buf, rgb2col(255, 127, 0));
      fill_rect(0, 400-16, 30, 8, palette[mapbmp[ms_map_y][ms_map_x]]);
      snprintf(buf, sizeof(buf), "%s", tilemap);
      tr_draw_str(320-strlen(buf)*8, 400-16, buf, rgb2col(255, 127, 0));
    }
  }
  /* render times */
  {
    char buf[128];
    snprintf(buf, sizeof(buf), "LTIME:%02u; RTIME:%02u; TIME:%02u", rle-stt, dre-rle, dre-stt);
    tr_draw_str(320-strlen(buf)*8, 0, buf, rgb2col(255, 255, 255));
    snprintf(buf, sizeof(buf), "AS:%04d  AE:%04d", light_as, light_ae);
    tr_draw_str(320-strlen(buf)*8, 8, buf, rgb2col(255, 255, 0));
  }
  /* set update flag */
  sdl_frame_changed = 1;
}


////////////////////////////////////////////////////////////////////////////////
static void process_keyup (SDL_Event *ev) {
  switch (ev->key.keysym.sym) {
    case SDLK_KP_8: player[0].keys[PL_KEY_UP] = 0; break;
    case SDLK_KP_4: player[0].keys[PL_KEY_LEFT] = 0; break;
    case SDLK_KP_6: player[0].keys[PL_KEY_RIGHT] = 0; break;
    case SDLK_KP_2: player[0].keys[PL_KEY_DOWN] = 0; break;
    case SDLK_LCTRL: player[0].keys[PL_KEY_FIRE] = 0; break;
  }
}


static void process_keydown (SDL_Event *ev) {
  if (ev->key.keysym.sym == SDLK_RETURN && (ev->key.keysym.mod&KMOD_ALT)) { switch_fullscreen(); return; }
  if (ev->key.keysym.sym == SDLK_ESCAPE) {
    post_quit_message();
    return;
  }
  /* */
  if (ev->key.keysym.sym == SDLK_TAB && contouring) {
    while (theo_contour_step(&tcd)) ;
    contouring = 0;
    return;
  }
  /* */
  switch (ev->key.keysym.sym) {
    case SDLK_KP_8: player[0].keys[PL_KEY_UP] = 1; break;
    case SDLK_KP_4: player[0].keys[PL_KEY_LEFT] = 1; break;
    case SDLK_KP_6: player[0].keys[PL_KEY_RIGHT] = 1; break;
    case SDLK_KP_2: player[0].keys[PL_KEY_DOWN] = 1; break;
    case SDLK_LCTRL: player[0].keys[PL_KEY_FIRE] = 1; break;
  }
  /* */
  if (ev->key.keysym.sym == SDLK_F1) { hilight_passages ^= 0x01; return; }
  if (ev->key.keysym.sym == SDLK_F2) { hilight_passages ^= 0x02; return; }
  if (ev->key.keysym.sym == SDLK_F3) { show_palette ^= 1; return; }
  if (ev->key.keysym.sym == SDLK_F5) { filter_mapbmp(); return; }
  if (ev->key.keysym.sym == SDLK_F8) { map_detect_lights(); return; }
  if (ev->key.keysym.sym == SDLK_F9) { no_lights ^= 1; return; }
  if (ev->key.keysym.sym == SDLK_o) { show_orders ^= 1; return; }
  /* */
  if (level_file_name == NULL) {
    int new_level = 0;
    if (ev->key.keysym.sym == SDLK_KP_PLUS) {
      if (episode < 3) {
        ++episode;
        mission = 0;
        load_mission_level(episode, mission);
        new_level = 1;
      }
    }
    if (ev->key.keysym.sym == SDLK_KP_MINUS) {
      if (episode > 0) {
        --episode;
        mission = 0;
        load_mission_level(episode, mission);
        new_level = 1;
      }
    }
    if (ev->key.keysym.sym == SDLK_LEFT) {
      if (mission > 0) {
        --mission;
        load_mission_level(episode, mission);
        new_level = 1;
      }
    }
    if (ev->key.keysym.sym == SDLK_RIGHT) {
      new_level = 1;
      ++mission;
      if (load_mission_level(episode, mission) < 0) {
        --mission;
        load_mission_level(episode, mission);
      }
    }
    if (new_level) {
      prepare_loaded_level();
      contouring = 0;
      contour_count = 0;
      return;
    }
  }
  /* light radius */
  if (ev->key.keysym.sym == SDLK_1 || ev->key.keysym.sym == SDLK_2) {
    light_radius += (ev->key.keysym.sym == SDLK_1 ? -1 : 1);
    if (light_radius < 5) light_radius = 5;
    if (light_radius > 250) light_radius = 250;
  }
  /* light start angle */
  if (ev->key.keysym.sym == SDLK_q || ev->key.keysym.sym == SDLK_w) {
    light_as += (ev->key.keysym.sym == SDLK_q ? -1 : 1);
  }
  /* light end angle */
  if (ev->key.keysym.sym == SDLK_a || ev->key.keysym.sym == SDLK_s) {
    light_ae += (ev->key.keysym.sym == SDLK_a ? -1 : 1);
  }
}


static void process_mouse (SDL_Event *ev) {
  if (ev->button.button == SDL_BUTTON_RIGHT) {
    if (ev->button.x >= 0 && ev->button.y >= 0 &&
        ev->button.x < MAP_RES_X && ev->button.y < MAP_RES_Y) {
      light_x = ev->button.x;
      light_y = ev->button.y;
      return;
    }
  }
  if (ev->button.button == SDL_BUTTON_MIDDLE) {
    if (!contouring) {
      if (ev->button.x >= 0 && ev->button.y >= 0 &&
          ev->button.x < MAP_RES_X && ev->button.y < MAP_RES_Y) {
        const Uint8 *ks = SDL_GetKeyboardState(NULL);
        tcd.x = ev->button.x;
        tcd.y = ev->button.y;
        tcd.four_corners = (ks[SDL_SCANCODE_LCTRL] != 0);
        contour_no_optimize = (ks[SDL_SCANCODE_LALT] == 0);
        tcd.is_pixel_filled = is_pix_filled;
        tcd.add_pixel = add_pix;
        switch (theo_contour_check_start(&tcd)) {
          case 0: break; /* not on wall */
          case -1: /* inside wall, look for perimeter */
            /*
            while (tcd.y >= 0 && tcd.is_pixel_filled(tcd.x, tcd.y)) ++tcd.y;
            --tcd.y;
            */
            for (;;) {
              while (tcd.x >= 0 && tcd.is_pixel_filled(tcd.x, tcd.y)) --tcd.x;
              ++tcd.x;
              if (tcd.y == 0) break;
              if (tcd.is_pixel_filled(tcd.x, tcd.y+1)) break;
              --tcd.y;
            }
            if (theo_contour_check_start(&tcd) <= 0) break;
            /* fallthru */
          default:
            contouring = 1;
            contour_count = 0;
            next_contour_step_time = 0; /* make first step immideately */
        }
      }
    } else {
      while (theo_contour_step(&tcd)) ;
      contouring = 0;
      return;
    }
  }
}


static void process_motion (SDL_Event *ev) {
  if (ev->motion.x >= 0 && ev->motion.y >= 0 && ev->motion.x < MAP_RES_X && ev->motion.y < MAP_RES_Y) {
    ms_map_x = ev->motion.x;
    ms_map_y = ev->motion.y;
    if (ev->motion.state&SDL_BUTTON_RMASK) {
      light_x = ev->motion.x;
      light_y = ev->motion.y;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
#define FPS  (18)

static SDL_TimerID sdl_tidframe = 0;


////////////////////////////////////////////////////////////////////////////////
enum {
  EVT_SHOW_FRAME = 1,
  EVT_MESSAGE_RESET
};


////////////////////////////////////////////////////////////////////////////////
static Uint32 timer_cb (Uint32 interval, void *param) {
  SDL_Event event;
  event.type = SDL_USEREVENT;
  event.user.code = EVT_SHOW_FRAME;
  event.user.data1 = NULL;
  event.user.data2 = NULL;
  SDL_PushEvent(&event);
  return interval;
}


////////////////////////////////////////////////////////////////////////////////
static void main_loop (void) {
  SDL_Event ev;
  rebuild_screen();
  paint_screen();
  sdl_tidframe = SDL_AddTimer(1000/FPS, timer_cb, NULL);
  while (SDL_WaitEvent(&ev)) {
    if (ev.type == SDL_QUIT) break;
    switch (ev.type) {
      case SDL_KEYDOWN: process_keydown(&ev); break;
      case SDL_KEYUP: process_keyup(&ev); break;
      case SDL_MOUSEBUTTONDOWN: process_mouse(&ev); break;
      case SDL_MOUSEMOTION: process_motion(&ev); break;
      case SDL_USEREVENT:
        switch (ev.user.code) {
          case EVT_SHOW_FRAME:
            //if (!sdl_frame_changed) rebuild_screen();
            rebuild_screen();
            if (sdl_frame_changed) paint_screen();
            break;
        }
        break;
      case SDL_WINDOWEVENT:
        switch (ev.window.event) {
          case SDL_WINDOWEVENT_EXPOSED:
            paint_screen();
            break;
        }
    }
  }
  if (sdl_tidframe != 0) SDL_RemoveTimer(sdl_tidframe);
}


////////////////////////////////////////////////////////////////////////////////
static void process_options (int argc, char *argv[], int onlydbg) {
  int nomoreopts = 0;
  for (int f = 1; f < argc; ++f) {
    const char *arg = argv[f];
    if (!nomoreopts) {
      if (strcmp(arg, "--") == 0) { nomoreopts = 1; continue; }
      if (arg[0] == '-') {
        ++arg;
        if (strcmp(arg, "S") == 0) {
          dlogf("sound debug enabled\n");
          //optSndSyncDebug = 1;
          continue;
        }
        if (arg[0] == '-') ++arg;
        if (strcmp(arg, "wrap") == 0) {
          if (++f >= argc) { fprintf(stderr, "missing argument for command line option: '%s'", argv[f-1]); continue; }
          arg = argv[f];
          //eXmX or eXmXX
          if (toupper(arg[0]) == 'E' && toupper(arg[2]) == 'M' &&
              arg[1] >= '0' && arg[1] <= '3' &&
              arg[3] >= '0' && arg[3] <= '9' &&
              ((arg[4] != 0 && arg[4] >= '0' && arg[4] <= '9' && arg[5] == 0) || (arg[4] == 0))) {
            int n = (arg[3]-'0');
            if (arg[4] != 0) n = n*10+(arg[4]-'0');
            if (n < 0 || n > 16) { fprintf(stderr, "invalid level for 'wrap': '%s'", arg); continue; }
            episode = arg[1]-'0';
            mission = n;
            dlogf("[%s]: e=%d; m=%d", arg, episode, mission);
          } else {
            static char buf[8192];
            ios_t *fl;
            sprintf(buf, "lvl/%s.lvl", arg);
            fl = file_ropen(buf);
            if (fl == NULL) {
              fprintf(stderr, "can't find level '%s'", arg);
            } else {
              ios_close(fl);
              if (level_file_name != NULL) free(level_file_name);
              level_file_name = strdup(buf);
            }
          }
          continue;
        }
        fprintf(stderr, "unknown command line option: '%s'", argv[f]);
        continue;
      }
    }
    // snapshot name
  }
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  int res;
  //
  videolib_args(&argc, argv);
  process_options(argc, argv, 1);
  //
  if (videolib_init("Threat/SDL") < 0) {
    fprintf(stderr, "FATAL: can't initialize SDL!");
    exit(1);
  }
  //
  lb_monster_hit_by_light = monster_hit_by_light;
  //
  memset(palette, 0, sizeof(palette));
  //
  if (load_font("fnt/font1.fon") != 0) {
    fprintf(stderr, "FATAL: can't load font data!");
    exit(1);
  }
  if (load_tpcx(gfxmonsters, "spr/enemies.dat") != 0) {
    fprintf(stderr, "FATAL: can't load sprite data!");
    exit(1);
  }
  if (load_tpcx(gfxother, "spr/hum1218.dat") != 0) {
    fprintf(stderr, "FATAL: can't load sprite data!");
    exit(1);
  }
  if (load_tpcx(gfxicons, "spr/icons.dat") != 0) {
    fprintf(stderr, "FATAL: can't load sprite data!");
    exit(1);
  }
  //
  if (level_file_name != NULL) {
    res = load_custom_level(level_file_name);
  } else {
    res = load_mission_level(episode, mission);
  }
  if (res < 0) exit(1);
  prepare_loaded_level();
  //
  main_loop();
  //
  return 0;
}
