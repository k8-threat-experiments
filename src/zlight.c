#include "zlight.h"

#include "common.h"
#include "zpolymod.h"
#include "tlevel.h"
#include "tgfx.h"


////////////////////////////////////////////////////////////////////////////////
static Uint16 sqrttbl[200000];


/* n: [0..(1<<30-1)] */
static inline unsigned int isqrt0 (unsigned int n) {
  unsigned int s, t;
#define ISQRT_SQRT_BIT(k)  if (n >= (t = (s+(1<<(k-1)))<<(k+1))) { n -= t; s |= 1<<k; }
  s = 0;
  ISQRT_SQRT_BIT(14); ISQRT_SQRT_BIT(13); ISQRT_SQRT_BIT(12); ISQRT_SQRT_BIT(11); ISQRT_SQRT_BIT(10);
  ISQRT_SQRT_BIT(9); ISQRT_SQRT_BIT(8); ISQRT_SQRT_BIT(7); ISQRT_SQRT_BIT(6); ISQRT_SQRT_BIT(5);
  ISQRT_SQRT_BIT(4); ISQRT_SQRT_BIT(3); ISQRT_SQRT_BIT(2); ISQRT_SQRT_BIT(1);
  if (n > s<<1) s |= 1UL;
#undef ISQRT_SQRT_BIT
  return s;
}


__attribute__((constructor)) static void _ctor_sqrttbl (void) {
  for (size_t f = 0; f < sizeof(sqrttbl)/sizeof(sqrttbl[0]); ++f) sqrttbl[f] = isqrt0(f);
}


static inline unsigned int isqrt (unsigned int n) {
  return (n < sizeof(sqrttbl)/sizeof(sqrttbl[0]) ? sqrttbl[n] : isqrt0(n));
}


/*
static ccBool r_lights_additive = CC_FALSE;
CONVAR_BOOL(r_lights_additive, r_lights_additive, 0, "additive intensity lights?")
*/
static int r_lights_additive = 0;


////////////////////////////////////////////////////////////////////////////////
void (*lb_monster_hit_by_light) (int mnum, int x, int y, int alpha);


////////////////////////////////////////////////////////////////////////////////
/* lightbuf format (bytes):
 * (b,g,r,a) triplets
 * (b,g,r) are premultiplied (wow! i'm smart!)
 */
static Uint32 lightbuf[MAP_RES_Y*MAP_RES_X]; /* keeps bgra values */


////////////////////////////////////////////////////////////////////////////////
int light_dump = 0;
int player_light = 0; /* is this player light? player light lights monsters */


////////////////////////////////////////////////////////////////////////////////
/* current light parameters for tracer */
static int z_light_mindist = 0;
static int lcx, lcy, lrad;
static Uint8 lalpha;
static Uint32 lbgr;


////////////////////////////////////////////////////////////////////////////////
static inline void lb_add_rgb (int x, int y, Uint32 bgr, Uint8 alpha) {
  Uint32 *pos = lightbuf+320*y+x, aa;
  if (!r_lights_additive) {
    aa = K8MAX(alpha, (pos[0]>>24));
  } else {
    aa = byteclamp((pos[0]>>24)+alpha);
  }
  // premultiply light color
  if (bgr) {
    Uint32 add, add_nc, cbits, cmask;
    bgr =
      ((((bgr&0xff00ff)*(alpha+1)+0x800080)>>8)&0xff00ff)|
      ((((bgr&0x00ff00)*(alpha+1)+0x008000)>>8)&0x00ff00);
    // you are not expected to understand this
    // but if you are really curious, this is
    // 8-bit rgb adding with clamping
    add = (pos[0]&0xffffffu)+bgr;
    add_nc = (pos[0]&0xffffffu)^bgr;
    cbits = (add^add_nc)&0x01010100;
    cmask = cbits-(cbits>>8);
    bgr = ((add^cbits)|cmask)&0xffffffu;
    *pos = bgr|(aa<<24);
  } else {
    *pos = (pos[0]&0x00ffffffu)|(aa<<24);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void lb_put_light_point (int x, int y) {
  int dst = isqrt((x-lcx)*(x-lcx)+(y-lcy)*(y-lcy));
  if (dst >= z_light_mindist) {
    int a = lalpha-(dst*lalpha/lrad);
    if (a > 0 && mapbmp_orig[y][x] < 16) {
      if (a > 255) a = 255;
      //a = 200;
      if (a > 128) a = 128;
      if (player_light && monbmp[y][x]) {
        /* player light hits some monster */
        lb_monster_hit_by_light(monbmp[y][x]-1, x, y, a);
      }
      lb_add_rgb(x, y, lbgr, a);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static Uint8 *lightmask = NULL; /* bitmap; bit 31 == leftmost pos */
static int lm_wdt = 0, lm_hgt = 0;


static void prepare_lightmask (void) {
  int wdt = lrad*2+1, hgt = wdt;
  if (wdt*hgt > lm_wdt*lm_hgt) {
    lightmask = realloc(lightmask, wdt*hgt*sizeof(lightmask[0]));
    if (lightmask == NULL) {
      fprintf(stderr, "ZLIGHT: out of memory for lightmask!\n");
      abort();
    }
  }
  lm_wdt = wdt;
  lm_hgt = hgt;
  memset(lightmask, 0, lm_wdt*lm_hgt*sizeof(lightmask[0]));
}


static inline void lb_light_plot (int x, int y) {
  int a;
  x -= lcx-lrad;
  y -= lcy-lrad;
  if (x < 0 || y < 0 || x >= lm_wdt || y >= lm_hgt) abort();
  a = lm_wdt*y+x;
  lightmask[a] = 1;
}


static void lb_light_hline (int x, int y, int len) {
  int a;
  x -= lcx-lrad;
  y -= lcy-lrad;
  if (x < 0 || y < 0 || x+len-1 >= lm_wdt || y >= lm_hgt) abort();
  a = lm_wdt*y+x;
  memset(lightmask+a, 1, len);
}


////////////////////////////////////////////////////////////////////////////////
static void realize_lightmask (void) {
  int sy = lcy-lrad;
  for (int dy = 0; dy < lrad*2; ++dy, ++sy) {
    int sx = lcx-lrad;
    int a = dy*lm_wdt;
    for (int dx = 0; dx < lrad*2; ++dx, ++a, ++sx) {
      if (lightmask[a]) lb_put_light_point(sx, sy);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static int lrad_sqr;


static int trace_point_count;
static struct {
  int x;
  int y;
} trace_plist[8192];


void t_draw_line_no_last (int x0, int y0, int x1, int y1) {
  int dx =  K8ABS(x1-x0), sx = (x0 < x1 ? 1 : -1);
  int dy = -K8ABS(y1-y0), sy = (y0 < y1 ? 1 : -1);
  int err = dx+dy, e2; /* error value e_xy */
  for (;;) {
    if (x0 == x1 && y0 == y1) break;
    //put_pixel(x0, y0, col);
    if (x0 >= 0 && y0 >= 0 && x0 < MAP_RES_X && y0 < MAP_RES_Y) {
      //lb_put_light_point(x0, y0);
      //lb_add_rgb(x0, y0, lbgr, 255);
      lb_light_plot(x0, y0);
    }
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


static void trace_start (void) {
  lrad_sqr = lrad*lrad;
  trace_point_count = 0;
  prepare_lightmask();
}


static void trace_end (void) {
  if (trace_point_count > 1 && trace_plist[trace_point_count-1].x == trace_plist[0].x && trace_plist[trace_point_count-1].y == trace_plist[0].y) {
    --trace_point_count;
  }
  if (trace_point_count > 2) {
    int op = trace_point_count-1;
    for (int pp = 0; pp < trace_point_count; op = pp++) {
      t_draw_line_no_last(trace_plist[op].x, trace_plist[op].y, trace_plist[pp].x, trace_plist[pp].y);
    }
  }
}


static inline void trace_add_point (int x, int y) {
  if (!trace_point_count || trace_plist[trace_point_count-1].x != x || trace_plist[trace_point_count-1].y != y) {
    trace_plist[trace_point_count].x = x;
    trace_plist[trace_point_count].y = y;
    ++trace_point_count;
  }
  polymod_add_vertex(x, y);
}


static void trace_to_point (int x1, int y1) {
  int x0 = lcx, y0 = lcy;
  int dx =  K8ABS(x1-x0), sx = (x0 < x1 ? 1 : -1);
  int dy = -K8ABS(y1-y0), sy = (y0 < y1 ? 1 : -1);
  int err = dx+dy, e2; /* error value e_xy */
  int ox = x0, oy = y0, dst = lrad_sqr;
  for (;;) {
    if ((x0-lcx)*(x0-lcx)+(y0-lcy)*(y0-lcy) >= dst) {
      trace_add_point(x0, y0);
      break;
    }
    if (mapbmp_orig[y0][x0] >= 16) {
      trace_add_point(ox, oy);
      break;
    }
    ox = x0, oy = y0;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


static inline double deg2rad (double angle) {
  return angle*0.017453292519943295f; // (angle/180)*Math.PI;
}


enum {
  EDGE_RIGHT  = 0x00,
  EDGE_BOTTOM = 0x01,
  EDGE_LEFT   = 0x02,
  EDGE_TOP    = 0x03
};


static const char *ename[4] = {"RIGHT","BOTTOM","LEFT","TOP"};


/* find intersection point between square (lcx-lrad,lcy-lrad)-(lcx+lrad,lcy+lrad)
 * and ray originating from (lcx,lcy) fired at angle 'angle' degrees;
 * also return edge code */
static void ray_edge_intersection (int angle, int *ix, int *iy, int *edge) {
  int outcode, ex, ey;
  double rad;
  //angle = ((angle%360)+360)%360;
  /* easy angles */
  switch (angle) {
    case   0: *ix = lcx+lrad; *iy = lcy;      *edge = EDGE_RIGHT; return;
    case  45: *ix = lcx+lrad; *iy = lcy+lrad; *edge = EDGE_RIGHT/*|EDGE_BOTTOM*/; return;
    case  90: *ix = lcx;      *iy = lcy+lrad; *edge = EDGE_BOTTOM; return;
    case 135: *ix = lcx-lrad; *iy = lcy+lrad; *edge = /*EDGE_LEFT|*/EDGE_BOTTOM; return;
    case 180: *ix = lcx-lrad; *iy = lcy;      *edge = EDGE_LEFT; return;
    case 225: *ix = lcx-lrad; *iy = lcy-lrad; *edge = EDGE_LEFT/*|EDGE_TOP*/; return;
    case 270: *ix = lcx;      *iy = lcy-lrad; *edge = EDGE_TOP; return;
    case 315: *ix = lcx+lrad; *iy = lcy-lrad; *edge = /*EDGE_RIGHT|*/EDGE_TOP; return;
  }
  /* now determine outcode */
  /* 316..359; 0..45: right side */
  /* 46..135: bottom side */
  /* 136..225: left side */
  /* 226..315: top side */
  if (angle >= 315 || angle <= 45) outcode = EDGE_RIGHT;
  else if (angle >= 46 && angle <= 135) outcode = EDGE_BOTTOM;
  else if (angle >= 136 && angle <= 225) outcode = EDGE_LEFT;
  else if (angle >= 226 && angle <= 315) outcode = EDGE_TOP;
  else abort(); /* the thing that shoild not be */
  /* set ex and ey to reasonable big value */
  rad = deg2rad(angle);
  ex = lcx+cos(rad)*1000;//(lrad*2+100);
  ey = lcy+sin(rad)*1000;//(lrad*2+100);
  /* and find intersection point */
  /* use formulas y = y0+slope*(x-x0), x = x0+(1/slope)*(y-y0) */
  if (outcode == EDGE_TOP) {
    *ix = lcx+(ex-lcx)*((lcy-lrad)-lcy)/(ey-lcy);
    *iy = lcy-lrad;
  } else if (outcode == EDGE_BOTTOM) {
    *ix = lcx+(ex-lcx)*((lcy+lrad)-lcy)/(ey-lcy);
    *iy = lcy+lrad;
  } else if (outcode == EDGE_RIGHT) {
    *iy = lcy+(ey-lcy)*((lcx+lrad)-lcx)/(ex-lcx);
    *ix = lcx+lrad;
  } else if (outcode == EDGE_LEFT) {
    *iy = lcy+(ey-lcy)*((lcx-lrad)-lcx)/(ex-lcx);
    *ix = lcx-lrad;
  } else {
    abort(); /* the thing that shoild not be */
  }
  *edge = outcode;
}


/******************************************************************************/
/* funny recursive 'shadowcasting' algo from roguebasin                       */
/******************************************************************************/
static inline int light_is_blocked (int x, int y) {
  return (x < 0 || y < 0 || x >= MAP_RES_X || y >= MAP_RES_Y || mapbmp_orig[y][x] >= 16);
}


static int lsc_xx, lsc_xy, lsc_yx, lsc_yy;


// recursive lightcasting function
static void light_octant (int cx, int cy, int row, double start, double end) {
  if (start >= end) {
    double new_start;
    double l_slope, r_slope;
    int dst = lrad_sqr;
    int xx = lsc_xx, xy = lsc_xy, yx = lsc_yx, yy = lsc_yy;
    for (int j = row; j <= lrad; ++j) {
      int dx = -j-1;
      int dy = -j;
      int dysqr = dy*dy;
      int blocked = 0;
      int x = cx+dx*xx+dy*xy;
      int y = cy+dx*yx+dy*yy;
      double dyp5 = dy+0.5;
      double dym5 = dy-0.5;
      while (dx++ <= 0) {
        // translate the dx, dy coordinates into map coordinates
        x += xx;
        y += yx;
        // l_slope and r_slope store the slopes of the left and right
        // extremities of the tile we're considering
        l_slope = (dx-0.5)/dyp5;
        if (end > l_slope) break;
        r_slope = (dx+0.5)/dym5;
        if (start >= r_slope) {
          // our light beam is touching this tile; light it
          int blk = light_is_blocked(x, y);
          if (blocked) {
            // we're scanning a row of blocked squares
            if (blk) {
              new_start = r_slope;
            } else {
              blocked = 0;
              start = new_start;
              if (dx*dx+dysqr < dst) lb_light_plot(x, y);
            }
          } else {
            if (blk) {
              if (j < lrad) {
                // this is a blocking tile, start a child scan
                blocked = 1;
                light_octant(cx, cy, j+1, start, l_slope);
                new_start = r_slope;
              }
            } else {
              if (dx*dx+dysqr < dst) lb_light_plot(x, y);
            }
          }
        }
      }
      // row is scanned; do next row unless last tile was blocked
      if (blocked) break;
    }
  }
}
/******************************************************************************/


/* lcx, lcy, lrad: light parameters */
/* as: start angle, ae: end ange; same: do full circle */
static void trace_light (int as, int ae) {
/*
  //int ang = (int)(atan2f(ye-cy, xe-cx)*180.0f/M_PI);
  //ang = (ang+360)%360;
  int ang = atan2i(ye-cy, xe-cx);
*/
  int asx = ((as%360)+360)%360;
  int aex = ((ae%360)+360)%360;
  trace_start();
  if (asx == aex) {
    /* do full circle (square, actually) */
#if 1
    /* top */
    for (int d = -lrad; d < lrad; ++d) trace_to_point(lcx+d, lcy-lrad);
    /* right */
    for (int d = -lrad; d < lrad; ++d) trace_to_point(lcx+lrad, lcy+d);
    /* bottom */
    for (int d = lrad; d > -lrad; --d) trace_to_point(lcx+d, lcy+lrad);
    /* left */
    for (int d = lrad; d > -lrad; --d) trace_to_point(lcx-lrad, lcy+d);
#else
    // multipliers for transforming coordinates to other octants
    static const int mult[4][8] = {
      {1, 0, 0,-1,-1, 0, 0, 1},
      {0, 1,-1, 0, 0,-1, 1, 0},
      {0, 1, 1, 0, 0,-1,-1, 0},
      {1, 0, 0, 1,-1, 0, 0,-1},
    };
    for (int oct = 0; oct < 8; ++oct) {
      lsc_xx = mult[0][oct];
      lsc_xy = mult[1][oct];
      lsc_yx = mult[2][oct];
      lsc_yy = mult[3][oct];
      light_octant(lcx, lcy, 1, 1.0, 0.0);
    }
    lb_light_plot(lcx, lcy);
#endif
  } else {
    /* do light sector */
    int sx, sy, sedge;
    int ex, ey, eedge;
    int dir = (as < ae ? 1 : -1); /* -1: counter-clockwise; 1: clockwise */
    /* now determine starting and ending coords */
    ray_edge_intersection(asx, &sx, &sy, &sedge);
    ray_edge_intersection(aex, &ex, &ey, &eedge);
    /* now process edges */
    if (light_dump) {
      fprintf(stderr, "---------------------------\n(%d,%d)-(%d,%d)\n", lcx-lrad, lcy-lrad, lcx+lrad, lcy+lrad);
      fprintf(stderr, "as=%d; asx=%d; ae=%d; aex=%d; sx=%d; sy=%d; sedge=%s; ex=%d; ey=%d; eedge=%s\n", as, asx, ae, aex, sx, sy, ename[sedge], ex, ey, ename[eedge]);
    }
    trace_add_point(lcx, lcy);
    for (int f = 0; f <= 4; ++f) {
      int delta, dx, dy, eex, eey;
      /* determine side delta */
      delta = (sedge == EDGE_RIGHT || sedge == EDGE_TOP ? dir : -dir);
      switch (sedge) {
        case EDGE_RIGHT: eex = lcx+lrad; eey = lcy+lrad*delta; dx = 0; dy = delta; break;
        case EDGE_BOTTOM: eex = lcx+lrad*delta; eey = lcy+lrad; dx = delta; dy = 0; break;
        case EDGE_LEFT: eex = lcx-lrad; eey = lcy+lrad*delta; dx = 0; dy = delta; break;
        case EDGE_TOP: eex = lcx+lrad*delta; eey = lcy-lrad; dx = delta; dy = 0; break;
        default: abort(); /* the thing that shoild not be */
      }
      if (light_dump) fprintf(stderr, "EDGE: %s; sx=%d; sy=%d; xs=%d; ys=%d; delta=%d; dir=%d\n", ename[sedge], sx, sy, K8SIGN(ex-sx), K8SIGN(ey-sy), delta, dir);
      if (sedge == eedge && (f > 0 || dir == 1)) {
        /* this is the last edge */
        if (light_dump) fprintf(stderr, "LAST EDGE! ex=%d; ey=%d; dx=%d; dy=%d\n", ex, ey, dx, dy);
        while (sx != ex || sy != ey) {
          trace_to_point(sx, sy);
          sx += dx;
          sy += dy;
        }
        /* that's all, folks */
        break;
      } else {
        /* this is not the last edge, trace it fully */
        if (light_dump) fprintf(stderr, " eex=%d; eey=%d; dx=%d; dy=%d\n", eex, eey, dx, dy);
        while (sx != eex || sy != eey) {
          trace_to_point(sx, sy);
          sx += dx;
          sy += dy;
        }
        /* go to the next side */
        sedge = (sedge+4+dir)&0x03;
      }
    }
  }
  trace_end();
}


////////////////////////////////////////////////////////////////////////////////
void lb_reset (void) {
  memset(lightbuf, 0, sizeof(lightbuf));
}


void lb_draw (void) {
  const Uint8 *msrc = (void *)mapbmp;
  const Uint32 *lbsrc = lightbuf;
  Uint32 *dest = sdl_vscr;
  for (size_t f = 0; f < MAP_RES_X*MAP_RES_Y; ++f) {
    Uint32 mbgr = (palette[*msrc]&0xffffff);
    if ((lbsrc[0]&0xff000000u) > 0) {
      /*if (mapbmp_orig[f/SCR_WIDTH][f%SCR_WIDTH] == msrc[0])*/ {
        // mix lights
        Sint32 alpha = 256-(lbsrc[0]>>24);
        if (mapbmp_orig[f/SCR_WIDTH][f%SCR_WIDTH] != msrc[0]) {
          alpha = 256-byteclamp((lbsrc[0]>>24)+50);
        }
        Uint32 drb = (mbgr&0xff00ff), dg = (mbgr&0x00ff00);
        // real pixel, do alpha
        drb = (drb+(((-drb)*alpha+0x800080)>>8))&0xff00ff;
        dg = (dg+(((-dg)*alpha+0x008000)>>8))&0x00ff00;
        // lights, premultiplied
        if (lbsrc[0]&0xffffffu) {
          Uint32 add, add_nc, cbits, cmask, bgr = (drb|dg);
          // you are not expected to understand this
          // but if you are really curious, this is
          // 8-bit rgb adding with clamping
          add = (lbsrc[0]&0xffffffu)+bgr;
          add_nc = (lbsrc[0]&0xffffffu)^bgr;
          cbits = (add^add_nc)&0x01010100;
          cmask = cbits-(cbits>>8);
          mbgr = ((add^cbits)|cmask)&0xffffffu;
        } else {
          mbgr = (drb|dg);
        }
      }
    } else {
      mbgr = 0;
    }
    *dest = mbgr;
    ++dest;
    ++msrc;
    ++lbsrc;
  }
}


////////////////////////////////////////////////////////////////////////////////
void render_warfog_sphere (int cx, int cy, int radius) {
  for (int y = cy-radius; y <= cy+radius; ++y) {
    for (int x = cx-radius; x <= cx+radius; ++x) {
      if (x >= 0 && y >= 0 && x < MAP_RES_X && y < MAP_RES_Y) {
        int dst = isqrt((x-cx)*(x-cx)+(y-cy)*(y-cy));
        int a = 255-(dst*255/radius);
        if (a > 0 && mapbmp_orig[y][x] >= 16) {
          if (a > 255) a = 255;
          lb_add_rgb(x, y, /*lbgr*/0, a);
        }
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
/* x, y: map coordinates (in pixels) */
/* alpha is actually light intensity */
void renderlight_sector (int x, int y, int radius, int as, int ae, Uint32 rgba) {
  //fprintf(stderr, "x=%d; y=%d; sx=%d; sy=%d\n", x, y, sx, sy);
  if (x-radius >= MAP_RES_X || x+radius < 0 || y-radius >= MAP_RES_Y || y+radius < 0) { z_light_mindist = 0; return; }
  if (radius < 2 /*|| fldhit1(x, y)*/ || (rgba&0xff000000u) == 0 || z_light_mindist >= radius) { z_light_mindist = 0; return; }
  if (mapbmp_orig[y][x] >= 16) return;
  lcx = x;
  lcy = y;
  lrad = radius;
  lalpha = rgba>>24;
  lbgr = (rgba&0xffffff);
  polymod_start();
  trace_light(as, ae);
  polymod_end();
  polymod_fill();
  realize_lightmask();
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int x;
  int y;
  int radius;
  int as;
  int ae;
  Uint32 rgba;
  int minrad;
} light_info_t;


#define MAX_LIGHTS  (4096)

static light_info_t lights[MAX_LIGHTS];
static int light_count;
static int level_light_count;


////////////////////////////////////////////////////////////////////////////////
void reset_level_lights (void) {
  level_light_count = light_count = 0;
}


void reset_lights (void) {
  light_count = level_light_count;
}


void fix_level_lights (void) {
  level_light_count = light_count;
}


int add_light_sector_mr (int x, int y, int radius, int as, int ae, Uint32 rgba, int minrad) {
  if (light_count < MAX_LIGHTS && radius >= 2 && (rgba&0xff000000u) > 0 && minrad <= radius) {
    lights[light_count].x = x;
    lights[light_count].y = y;
    lights[light_count].radius = radius;
    lights[light_count].as = as;
    lights[light_count].ae = ae;
    lights[light_count].rgba = rgba;
    lights[light_count].minrad = minrad;
    return light_count++;
  }
  return -1;
}


int add_light_sector (int x, int y, int radius, int as, int ae, Uint32 rgba) {
  return add_light_sector_mr(x, y, radius, as, ae, rgba, 0);
}


void render_lights (void) {
  const light_info_t *lt = lights;
  for (int f = light_count; f > 0; --f, ++lt) {
    z_light_mindist = lt->minrad;
    player_light = 0;
    renderlight_sector(lt->x, lt->y, lt->radius, lt->as, lt->ae, lt->rgba);
  }
}


////////////////////////////////////////////////////////////////////////////////
static __attribute__((constructor)) void _ctor_polymod_init (void) {
  //polymod_put_pixel = lb_put_light_point;
  polymod_hline = lb_light_hline;
  polymod_screen_width = MAP_RES_X;
  polymod_screen_height = MAP_RES_Y;
}
