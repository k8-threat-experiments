/* Theo Pavlidis' Algorithm */
/* http://www.imageprocessingplace.com/downloads_V3/root_downloads/tutorials/contour_tracing_Abeer_George_Ghuneim/theo.html */
typedef struct {
  /* the following must be set by user */
  int x, y;
  int four_corners;
  int (*is_pixel_filled) (int x, int y);
  void (*add_pixel) (int x, int y);
  /* internal tracer data */
  int sx, sy; /* starting x and y */
  int dir; /* current direction */
} theo_contour_data_t;


static int theo_contour_check_start (theo_contour_data_t *td) {
  if (!td->is_pixel_filled(td->x, td->y)) return 0;
  /* check pixels: left, left-down, right-down */
  //if (td->is_pixel_filled(td->x-1, td->y) || td->is_pixel_filled(td->x-1, td->y+1) || td->is_pixel_filled(td->x+1, td->y+1)) return -1; /* bad starting point */
  if (td->is_pixel_filled(td->x-1, td->y)) return -1; /* bad starting point */
  td->dir = 0;
  td->sx = td->x;
  td->sy = td->y;
  return 1;
}


/* 0: done; >0: not done */
static int theo_contour_step (theo_contour_data_t *td) {
  static const int dir_delta[4][2] = {
    { 0,-1}, /* 0: up */
    { 1, 0}, /* 1: right */
    { 0, 1}, /* 2: down */
    {-1, 0}, /* 3: left */
  };
  td->add_pixel(td->x, td->y);
  /* check up-left pixel, then up pixel, then up-right pixel */
  for (int rc = 0; ; ++rc) {
    int nx, ny;
    int ux = td->x+dir_delta[td->dir][0];
    int uy = td->y+dir_delta[td->dir][1];
    /* top-left pixel */
    nx = ux+dir_delta[(td->dir+3)&3][0];
    ny = uy+dir_delta[(td->dir+3)&3][1];
    if (td->is_pixel_filled(nx, ny)) {
      if (!td->four_corners) td->add_pixel(ux, uy); /* one step up */
      td->x = nx;
      td->y = ny;
      td->dir = (td->dir+3)&3; /* turn left */
      break;
    }
    /* top pixel */
    if (td->is_pixel_filled(ux, uy)) {
      td->x = ux;
      td->y = uy;
      if (td->four_corners) {
        if (td->x == td->sx && td->y == td->sy) return 0;
        /* check if top-top pixel is filled */
        ux = td->x+dir_delta[td->dir][0];
        uy = td->y+dir_delta[td->dir][1];
        if (!td->is_pixel_filled(ux, uy)) {
          /* it's empty, check if there is no top-left one and top-right one */
          if (!td->is_pixel_filled(ux+dir_delta[(td->dir+3)&3][0], uy+dir_delta[(td->dir+3)&3][1]) &&
              !td->is_pixel_filled(ux+dir_delta[(td->dir+1)&3][0], uy+dir_delta[(td->dir+1)&3][1])) {
            /* there will be turn anyway */
            rc = -1;
            continue;
          }
        }
      }
      break;
    }
    /* top-right pixel */
    nx = ux+dir_delta[(td->dir+1)&3][0];
    ny = uy+dir_delta[(td->dir+1)&3][1];
    if (td->is_pixel_filled(nx, ny)) {
      if (!td->four_corners) td->add_pixel(td->x+dir_delta[(td->dir+1)&3][0], td->y+dir_delta[(td->dir+1)&3][1]); /* one step right */
      td->x = nx;
      td->y = ny;
      break;
    }
    /* turn right */
    if (rc > 3) return 0; /* no place to go */
    td->dir = (td->dir+1)&3;
  }
  return (td->x != td->sx || td->y != td->sy);
}
