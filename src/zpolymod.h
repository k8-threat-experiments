/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef ZPOLYMOD_H
#define ZPOLYMOD_H


/* note that polygons can be concave, but verticies MUST be in clockwise order */
/* rasterizer WILL NOT omit 'right' and 'bottom' points */

/* x, y and x+len-1 will never be offscreen; len will always be positive */
typedef void (*polymod_hline_fn) (int x, int y, int len);

extern polymod_hline_fn polymod_hline;
extern int polymod_screen_width;
extern int polymod_screen_height;

extern void polymod_init (void); /* called once in program startup sequence */
extern void polymod_deinit (void); /* called once in program shutdown sequence */

extern void polymod_start (void); /* called for each new poly */
extern void polymod_add_vertex (int x, int y); /* will ignore duplicate verticies */
extern void polymod_end (void); /* called after the last vertex was added; note that you MUST not specify starting vertex twice! */

extern void polymod_fill (void); /* called after polymod_end() to fill resulting poly with polymod_hline() callback */


#endif
