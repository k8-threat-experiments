/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "tlevel.h"

#include "fileio.h"
#include "tgfx.h"


////////////////////////////////////////////////////////////////////////////////
const char *order_text[4] = {
  "Eliminate all monsters",
  "Destroy all objects",
  "Time limit",
  "Destination point",
};


Uint8 map[MAP_HEIGHT][MAP_WIDTH];
char tilemap[9];
char lvltitle[21];
char orders[4]; // indexed by order_type_t
int lvltime; // time limit (seconds?)
monster_wave_info_t mwaves[5];
explosive_info_t explos[10];
door_info_t doors[256];
point_t plrpos[3];
point_t behere_pos;
button_info_t buttons[256];


////////////////////////////////////////////////////////////////////////////////
static int load_level_data (const char *fname, int idx) {
  ios_t *fl = file_ropen(fname);
  if (fl != NULL) {
    Uint32 w, h, tl;
    Uint8 cnt;
    char tmpbuf[10];
    if (ios_seek(fl, idx*2000) != 0) goto error;
    // header
    if (file_read_u16(fl, &w) != 0) goto error;
    if (file_read_u16(fl, &h) != 0) goto error;
    if (w != MAP_WIDTH || h != MAP_HEIGHT) goto error;
    if (file_read_pstr(fl, tilemap, 8) != 0) goto error;
    if (ios_read(fl, map, w*h) != w*h) goto error;
    // unused bytes
    if (ios_read(fl, tmpbuf, 10) != 10) goto error;
    // monster waves
    memset(mwaves, 0, sizeof(mwaves));
    for (int wn = 0; wn < 5; ++wn) {
      if (ios_read(fl, &cnt, 1) != 1) goto error;
      if (cnt > 10) goto error;
      mwaves[wn].count = cnt;
      if (cnt > 0) {
        for (int mn = 0; mn < cnt; ++mn) {
          monster_info_t *m = &mwaves[wn].m[mn];
          Uint8 mt, mai;
          Uint32 x, y;
          if (ios_read(fl, &mt, 1) != 1) goto error;
          if (mt >= MT_MAX || mt == MT_NONE) goto error;
          if (ios_read(fl, &mai, 1) != 1) goto error;
          if (mai >= MAI_MAX) goto error;
          if (file_read_u16(fl, &x) != 0) goto error;
          if (file_read_u16(fl, &y) != 0) goto error;
          m->type = mt;
          m->ai = mai;
          m->pos.x = x;
          m->pos.y = y;
          //dlogf("wave #%d; monster #%d; t=%2u; ai=%u; x=%u; y=%u", wn, mn, mt, mai, x/16, y/16);
          if (m->ai == 1) {
            for (int pn = 0; pn < 8; ++pn) {
              if (file_read_u16(fl, &x) != 0) goto error;
              if (file_read_u16(fl, &y) != 0) goto error;
              m->path[pn].x = x;
              m->path[pn].y = y;
            }
          }
        }
      }
    }
    // orders
    for (int f = 0; f < 4; ++f) {
      if (ios_read(fl, &orders[f], 1) != 1) goto error;
    }
    // explosives
    if (ios_read(fl, &cnt, 1) != 1) goto error;
    for (int en = 0; en < 10; ++en) {
      Uint32 x, y, sz;
      if (file_read_u16(fl, &x) != 0) goto error;
      if (file_read_u16(fl, &y) != 0) goto error;
      if (file_read_u16(fl, &sz) != 0) goto error;
      if (sz%16) sz = 0;
      explos[en].pos.x = x;
      explos[en].pos.y = y;
      explos[en].size = sz;
    }
    for (int en = cnt; en < 10; ++en) explos[en].size = 0;
    // time limit
    if (file_read_u16(fl, &tl) != 0) goto error;
    lvltime = tl;
    // doors
    if (ios_read(fl, &cnt, 1) != 1) goto error;
    memset(doors, 0, sizeof(doors));
    for (int dn = 0; dn < cnt; ++dn) {
      Uint32 x, y, t;
      if (file_read_u16(fl, &t) != 0) goto error;
      if (t == DT_NONE || t >= DT_MAX) goto error;
      if (file_read_u16(fl, &x) != 0) goto error;
      if (file_read_u16(fl, &y) != 0) goto error;
      doors[dn].pos.x = x;
      doors[dn].pos.y = y;
      doors[dn].type = t;
    }
    // music
    if (file_read_pstr(fl, tmpbuf, 8) != 0) goto error;
    // title
    if (file_read_pstr(fl, lvltitle, 20) != 0) goto error;
    // player positions
    for (int pn = 0; pn < 3; ++pn) {
      Uint32 x, y;
      if (file_read_u16(fl, &x) != 0) goto error;
      if (file_read_u16(fl, &y) != 0) goto error;
      plrpos[pn].x = x;
      plrpos[pn].y = y;
    }
    // behere_pos
    {
      Uint32 x, y;
      if (file_read_u16(fl, &x) != 0) goto error;
      if (file_read_u16(fl, &y) != 0) goto error;
      behere_pos.x = x;
      behere_pos.y = y;
    }
    // buttons
    if (ios_read(fl, &cnt, 1) != 1) goto error;
    memset(buttons, 0, sizeof(buttons));
    for (int bn = 0; bn < cnt; ++bn) {
      Uint8 x0, y0, x1, y1;
      if (ios_read(fl, &x0, 1) != 1) goto error;
      if (ios_read(fl, &y0, 1) != 1) goto error;
      if (ios_read(fl, &x1, 1) != 1) goto error;
      if (ios_read(fl, &y1, 1) != 1) goto error;
      buttons[bn].barea.x = x0*TILE_WIDTH;
      buttons[bn].barea.y = y0*TILE_HEIGHT;
      buttons[bn].barea.w = (x1-x0)*TILE_WIDTH;
      buttons[bn].barea.h = (y1-y0)*TILE_HEIGHT;
      if (ios_read(fl, &x0, 1) != 1) goto error;
      if (ios_read(fl, &y0, 1) != 1) goto error;
      if (ios_read(fl, &x1, 1) != 1) goto error;
      if (ios_read(fl, &y1, 1) != 1) goto error;
      buttons[bn].earea.x = x0*TILE_WIDTH;
      buttons[bn].earea.y = y0*TILE_HEIGHT;
      buttons[bn].earea.w = (x1-x0)*TILE_WIDTH;
      buttons[bn].earea.h = (y1-y0)*TILE_HEIGHT;
    }
    //dlogf("title: [%s]", lvltitle);
    ios_close(fl);
    return 0;
  }
error:
  if (fl != NULL) ios_close(fl);
  return -1;
}




////////////////////////////////////////////////////////////////////////////////
int load_level_tiles (void) {
  char tilefn[512];
  sprintf(tilefn, "tiles/%s.dat", tilemap);
  if (load_tpcx(tilegfx, tilefn) != 0) {
    fprintf(stderr, "ERROR: can't load tiles: '%s'\n", tilefn);
    return -1;
  }
  return 0;
}


int load_custom_level (const char *fname) {
  if (load_level_data(fname, 0) != 0) {
    fprintf(stderr, "ERROR: can't load level: '%s'\n", fname);
    return -1;
  }
  if (load_level_tiles() < 0) return -1;
  //memcpy(gfxbuf, tilegfx, 320*200);
  tr_draw_map_on_mapbmp_orig();
  return 0;
}


int load_mission_level (int episode, int mission) {
  static char fname[512];
  sprintf(fname, "lvl/%s%d.dat", (episode > 0 ? "lvl" : "bat"), (episode > 0 ? episode : 1));
  if (load_level_data(fname, mission) != 0) {
    fprintf(stderr, "ERROR: can't load level: E%dM%d\n", episode, mission);
    return -1;
  }
  if (load_level_tiles() < 0) return -1;
  //memcpy(gfxbuf, tilegfx, 320*200);
  tr_draw_map_on_mapbmp_orig();
  return 0;
}
