/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "fileio.h"

#include <unistd.h>


////////////////////////////////////////////////////////////////////////////////
static char bin_dir[8192];


static __attribute__((constructor)) void _ctor_bin_dir (void) {
  bin_dir[sizeof(bin_dir)-1] = 0; /* just in case */
  if (readlink("/proc/self/exe", bin_dir, sizeof(bin_dir)-1) < 0) {
    strcpy(bin_dir, ".");
  } else {
    char *p = strrchr(bin_dir, '/');
    if (p == NULL) strcpy(bin_dir, "."); else *p = '\0';
  }
}


////////////////////////////////////////////////////////////////////////////////
ios_t *file_ropen (const char *fname) {
  if (fname != NULL && fname[0] && fname[strlen(fname)-1] != '/') {
    char *buf;
    ios_t *fl;
    const char *fn = fname;
    if (fname[0] != '/') {
      buf = alloca(strlen(bin_dir)+strlen(fname)+4);
      sprintf(buf, "data/%s", fname);
      if (access(buf, R_OK) != 0) sprintf(buf, "%s/data/%s", bin_dir, fname);
      fn = buf;
    }
    if ((fl = malloc(sizeof(*fl))) == NULL) return NULL;
    if (ios_file(fl, fn, "r") == NULL) { free(fl); return NULL; }
    return fl;
  }
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
int file_read_u16 (ios_t *fl, Uint32 *r) {
  Uint32 res = 0;
  if (r != NULL) *r = 0;
  if (fl == NULL) return -1;
  for (int f = 0; f < 2; ++f) {
    Uint8 b;
    if (ios_read(fl, &b, 1) != 1) return -1;
    res |= (((Uint32)b)<<(f*8));
  }
  if (r != NULL) *r = res;
  return 0;
}


int file_read_pstr (ios_t *fl, void *dest, int slen) {
  if (slen > 0) {
    Uint8 l;
    char *d = (char *)dest;
    if (ios_read(fl, &l, 1) != 1) return -1;
    if (l > slen) l = slen;
    if (ios_read(fl, dest, slen) != slen) return -1;
    memset(d+l, 0, slen+1-l);
    return 0;
  }
  return -1;
}
