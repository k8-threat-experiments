#define CHECK_HLINE(_cx,_cy,_len)  do { \
  int lx = (_cx), ly = (_cy), l = (_len); \
  while (l-- > 0) { \
    if (map_get_pixel_orig(lx, ly) >= 16) { \
      if (++cnt > max_cnt) return 1; \
    } \
    ++lx; \
  } \
} while (0)


/*
static int check_circle_collision (int cx, int cy, int radius) {
  if (radius > 0) {
    int error = -radius, x = radius, y = 0, cnt = 0, max_cnt = 3;
    if (radius == 1) return (map_get_pixel_orig(cx, cy) >= 16);
    while (x >= y) {
      int last_y = y;
      error += y;
      ++y;
      error += y;
      CHECK_HLINE(cx-x, cy+last_y, 2*x+1);
      if (x != 0 && last_y != 0) CHECK_HLINE(cx-x, cy-last_y, 2*x+1);
      if (error >= 0) {
        if (x != last_y) {
          CHECK_HLINE(cx-last_y, cy+x, 2*last_y+1);
          if (last_y != 0 && x != 0) CHECK_HLINE(cx-last_y, cy-x, 2*last_y+1);
        }
        error -= x;
        --x;
        error -= x;
      }
    }
  }
  return 0;
}
*/


static int check_ellipse_collision (int x0, int y0, int x1, int y1) {
  int max_cnt = 5, cnt = 0;
  int a = K8ABS(x1-x0), b = K8ABS(y1-y0), b1 = b&1; /* values of diameter */
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  long err = dx+dy+b1*a*a; /* error of 1.step */
  int prev_y0 = -1, prev_y1 = -1;
  //max_cnt = K8MAX(a, b);
  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points... */
  if (y0 > y1) y0 = y1; /* ...exchange them */
  y0 += (b+1)/2; y1 = y0-b1; /* starting pixel */
  a *= 8*a; b1 = 8*b*b;
  do {
    int e2;
    if (y0 != prev_y0) { CHECK_HLINE(x0, y0, x1-x0+1); prev_y0 = y0; }
    if (y1 != y0 && y1 != prev_y1) { CHECK_HLINE(x0, y1, x1-x0+1); prev_y1 = y1; }
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } /* x step */
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  /* y step */
  } while (x0 <= x1);
  while (y0-y1 < b) {
    /* too early stop of flat ellipses a=1 */
    if (map_get_pixel_orig(x0-1, ++y0) >= 16) ++cnt; /* -> complete tip of ellipse */
    if (map_get_pixel_orig(x0-1, --y1) >= 16) ++cnt;
    return (cnt > max_cnt);
  }
  return 0;
}
