/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "tgfx.h"

#include "fileio.h"
#include "zlight.h"


////////////////////////////////////////////////////////////////////////////////
Uint32 palette[256];
Uint8 font[256][8];
Uint8 gfxmonsters[200][320];
Uint8 gfxother[200][320];
Uint8 gfxicons[200][320];
Uint8 tilegfx[200][320];

Uint8 mapbmp_orig[MAP_RES_Y][MAP_RES_X];
Uint8 mapbmp[MAP_RES_Y][MAP_RES_X];
Uint8 monbmp[MAP_RES_Y][MAP_RES_X];


////////////////////////////////////////////////////////////////////////////////
int load_font (const char *fname) {
  ios_t *fl = file_ropen(fname);
  int res = -1;
  if (fl != NULL) {
    if (ios_read(fl, font, sizeof(font)) == sizeof(font)) res = 0;
    ios_close(fl);
  }
  return res;
}


// gfxbuf should be 64000 bytes long
int load_tpcx (void *gfxbuf, const char *fname) {
  ios_t *fl = file_ropen(fname);
  if (fl != NULL) {
    Uint8 b;
    Uint8 *dest = (Uint8 *)gfxbuf;
    int pos = 0;
    while (pos < 320*200) {
      Uint8 c = 1;
      if (ios_read(fl, &b, 1) != 1) goto error;
      if ((b&0xc0) == 0xc0) {
        c = (b&0x3f);
        if (ios_read(fl, &b, 1) != 1) goto error;
      }
      while (pos < 320*200 && c-- > 0) dest[pos++] = b;
    }
    // check for palette and load it
    if (ios_read(fl, &b, 1) == 1 && b == 0x0c) {
      // palette
      for (int f = 0; f < 256; ++f) {
        Uint8 r, g, b;
        if (ios_read(fl, &r, 1) != 1) goto error;
        if (ios_read(fl, &g, 1) != 1) goto error;
        if (ios_read(fl, &b, 1) != 1) goto error;
        if (!(r == 0xff && g == 0x00 && b == 0xff)) palette[f] = rgb2col(r, g, b);
      }
    }
    ios_close(fl);
    return 0;
  }
error:
  ios_close(fl);
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
const monster_anim_info_t mainms[15] = {
  {0,       0, 0, 0}, // MT_NONE
  {26*0+0,  3, 1, 0}, // MT_MIRMUTTAJA
  {26*1+6,  1, 1, 0}, // MT_BOSS
  {26*10+20,2, 0, 1}, // MT_SHADOW
  {26*3+2,  1, 1, 0}, // MT_SLIMER
  {26*4+8,  2, 1, 0}, // MT_WORM
  {26*5+6,  2, 1, 0}, // MT_BUG
  {26*6+4,  3, 1, 0}, // MT_RENEGADE
  {26*7+10, 3, 1, 0}, // MT_STEELER
  {26*8+16, 2, 1, 0}, // MT_SPIDER
  {26*3+18, 1, 1, 0}, // MT_HUNTER
  {26*9+16, 1, 1, 0}, // MT_TURRET
  {26*10+4, 2, 0, 0}, // MT_VIPATTAJA
  {26*1+22, 1, 1, 0}, // MT_BRAIN
  {26*2+12, 1, 1, 0}, // MT_BUNKER
};


////////////////////////////////////////////////////////////////////////////////
void tr_draw_char (int x, int y, char ch, Uint32 clr) {
  Uint8 cc = (ch&0xff);
  for (int dy = 0; dy < 8; ++dy) {
    Uint8 b = font[cc][dy];
    for (int dx = 0; dx < 8; ++dx) {
      if (b&0x80) put_pixel(x+dx, y+dy, clr);
      b = (b&0x7f)<<1;
    }
  }
}


void tr_draw_str (int x, int y, const char *str, Uint32 clr) {
  int ox = x;
  while (str != NULL && *str) {
    switch (*str) {
      case '\n': y += 8; // fallthru
      case '\r': x = ox; break;
      default:
        tr_draw_char(x, y, *str++, clr);
        x += 8;
        break;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void tr_draw_tile_on_mapbmp_orig (int sx, int sy, Uint8 t) {
  int tx = (t%20)*TILE_WIDTH, ty = (t/20)*TILE_HEIGHT;
  if (sx < 0 || sy < 0 || sx >= MAP_WIDTH || sy >= MAP_HEIGHT || t >= 240) return;
  sx *= TILE_WIDTH;
  sy *= TILE_HEIGHT;
  for (int y = TILE_HEIGHT; y > 0; --y, ++sy, ++ty) memcpy(&mapbmp_orig[sy][sx], &tilegfx[ty][tx], TILE_WIDTH);
}


void tr_draw_map_on_mapbmp_orig (void) {
  for (int y = 0; y < MAP_HEIGHT; ++y) {
    for (int x = 0; x < MAP_WIDTH; ++x) {
      //dlogf("x=%d; y=%d; tile=%u", x, y, map[y][x]);
      tr_draw_tile_on_mapbmp_orig(x, y, map[y][x]);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void tr_draw_sprite_mono (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp, Uint32 clr) {
  int tx = (spr_idx%26)*MONSTER_WIDTH, ty = (spr_idx/26)*MONSTER_HEIGHT;
  if (sx < 0 || sy < 0 || sx >= MAP_RES_X-MONSTER_WIDTH || sy >= MAP_RES_Y-MONSTER_HEIGHT) return;
  bmp += 320*ty+tx;
  for (int y = MONSTER_HEIGHT; y > 0; --y, ++sy, ++ty) {
    for (int x = 0; x < MONSTER_WIDTH; ++x) {
      Uint8 c;
      if ((c = bmp[x])) put_pixel(sx+x, sy, clr);
    }
    bmp += 320;
  }
}


void tr_draw_sprite (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp) {
  int tx = (spr_idx%26)*MONSTER_WIDTH, ty = (spr_idx/26)*MONSTER_HEIGHT;
  if (sx < 0 || sy < 0 || sx >= MAP_RES_X-MONSTER_WIDTH || sy >= MAP_RES_Y-MONSTER_HEIGHT) return;
  bmp += 320*ty+tx;
  for (int y = MONSTER_HEIGHT; y > 0; --y, ++sy, ++ty) {
    for (int x = 0; x < MONSTER_WIDTH; ++x) {
      Uint8 c;
      if ((c = bmp[x])) put_pixel(sx+x, sy, palette[c]);
    }
    bmp += 320;
  }
}


void tr_draw_sprite_on_map (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp) {
  int tx = (spr_idx%26)*MONSTER_WIDTH, ty = (spr_idx/26)*MONSTER_HEIGHT;
  if (sx < 0 || sy < 0 || sx >= MAP_RES_X-MONSTER_WIDTH || sy >= MAP_RES_Y-MONSTER_HEIGHT) return;
  bmp += 320*ty+tx;
  for (int y = MONSTER_HEIGHT; y > 0; --y, ++sy, ++ty) {
    for (int x = 0; x < MONSTER_WIDTH; ++x) {
      Uint8 c;
      if ((c = bmp[x]) && x+sx >= 0 && y+sy >= 0 && x+sx < MAP_RES_X && sy < MAP_RES_Y && mapbmp_orig[sy][x+sx] < 16) {
        mapbmp[sy][x+sx] = c;
      }
    }
    bmp += 320;
  }
}


void tr_draw_sprite_on_mon (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp, Uint8 value) {
  int tx = (spr_idx%26)*MONSTER_WIDTH, ty = (spr_idx/26)*MONSTER_HEIGHT;
  if (sx < 0 || sy < 0 || sx >= MAP_RES_X-MONSTER_WIDTH || sy >= MAP_RES_Y-MONSTER_HEIGHT) return;
  bmp += 320*ty+tx;
  for (int y = MONSTER_HEIGHT; y > 0; --y, ++sy, ++ty) {
    for (int x = 0; x < MONSTER_WIDTH; ++x) {
      if (bmp[x] && x+sx >= 0 && y+sy >= 0 && x+sx < MAP_RES_X && sy < MAP_RES_Y) {
        monbmp[sy][x+sx] = value;
      }
    }
    bmp += 320;
  }
}


void tr_draw_pos (const point_t *pos, Uint32 c) {
  draw_rect(pos->x, pos->y, 16, 16, c);
}


////////////////////////////////////////////////////////////////////////////////
void filter_mapbmp (void) {
  int done;
  memcpy(mapbmp, mapbmp_orig, sizeof(mapbmp));
  do {
    done = 1;
    /* filter thin lines */
    for (int y = 0; y < MAP_RES_Y; ++y) {
      for (int x = 0; x < MAP_RES_X; ++x) {
        if (map_get_pixel(x, y) >= 16) {
          if ((map_get_pixel(x-1, y) < 16 && map_get_pixel(x+1, y) < 16) ||
              (map_get_pixel(x, y-1) < 16 && map_get_pixel(x, y+1) < 16)) {
            map_put_pixel(x, y, 0);
            done = 0;
          }
        }
      }
    }
    /* filter isolated pixels */
    for (int y = 0; y < MAP_RES_Y; ++y) {
      for (int x = 0; x < MAP_RES_X; ++x) {
        if (map_get_pixel(x, y) >= 16) {
          if (map_get_pixel(x-1, y) < 16 &&
              map_get_pixel(x+1, y) < 16 &&
              map_get_pixel(x, y-1) < 16 &&
              map_get_pixel(x, y+1) < 16) {
            map_put_pixel(x, y, 0);
            done = 0;
          }
        }
      }
    }
  } while (!done);
  memcpy(mapbmp_orig, mapbmp, sizeof(mapbmp));
}


void map_detect_lights (void) {
  memcpy(mapbmp, mapbmp_orig, sizeof(mapbmp));
  reset_level_lights();
  for (int y = 0; y < MAP_RES_Y; ++y) {
    for (int x = 0; x < MAP_RES_X; ++x) {
      if (map_get_pixel(x, y) == 43) {
        /* wall lamp */
        int dx = 0, dy = 0;
        for (int sy = -1; sy <= 1; ++sy) {
          for (int sx = -1; sx <= 1; ++sx) {
            if (sx != 0 && sy != 0) continue;
            if (sx == 0 && sy == 0) continue;
            if (sx == 0) {
              if (map_get_pixel(x, y-sy) == 45 && map_get_pixel(x-1, y-sy) == 43 && map_get_pixel(x+1, y-sy) == 43) {
                dx = sx;
                dy = sy;
                break;
              }
            } else {
              if (map_get_pixel(x-sx, y) == 45 && map_get_pixel(x-sx, y-1) == 43 && map_get_pixel(x-sx, y+1) == 43) {
                dx = sx;
                dy = sy;
                break;
              }
            }
          }
        }
        /* not a conventional light? */
        if (dx == 0 && dy == 0) {
          int good = 1;
          for (int sy = -1; sy <= 1; ++sy) {
            for (int sx = -1; sx <= 1; ++sx) {
              if (sx == 0 && sy == 0) continue;
              if (map_get_pixel(x+sx, y+sy) == 45 || map_get_pixel(x+sx, y+sy) == 43) {
                good = 0;
                break;
              }
            }
          }
          if (good) {
            for (int sy = -1; sy <= 1; ++sy) {
              for (int sx = -1; sx <= 1; ++sx) {
                if (sx == 0 || sy == 0) continue;
                if (map_get_pixel(x+sx, y+sy) < 16) {
                  dx = sx;
                  dy = sy;
                  break;
                }
              }
            }
            if (dx || dy) mapbmp[y][x] = 0;
          }
        } else {
          for (int sy = -1; sy <= 1; ++sy) {
            for (int sx = -1; sx <= 1; ++sx) {
              mapbmp[y+sy][x+sx] = 0;
            }
          }
        }
        if (dx || dy) add_light_sector(x, y, 42, 0, 0, rgba2col(255, 255, 0, 100));
      }
    }
  }
  fix_level_lights();
  memcpy(mapbmp_orig, mapbmp, sizeof(mapbmp));
}
