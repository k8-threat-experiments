// if start and end point are on the same x the distance is the difference in x
static inline float perp_dest (const point_t *p, const point_t *p0, const point_t *p1) {
  if (p0->x == p1->x) {
    return K8ABS(p->x-p0->x);
  } else {
    float slope = (float)(p1->y-p0->y)/(p1->x-p0->x);
    float intercept = p0->y-(slope*p0->x);
    return fabsf(slope*p->x-p->y+intercept)/sqrtf(slope*slope+1);
  }
}


/* return number of points in dest */
static int rdp_simplify (const point_t *src, int src_count, point_t *dest, float eps) {
  int res = 0;
again:
  if (src_count > 2) {
    const point_t *first = &src[0];
    const point_t *last = &src[src_count-1];
    int index = -1;
    float dist = 0.0f;
    for (int f = 1; f < src_count-1; ++f) {
      float cur_dist = perp_dest(&src[f], first, last);
      if (cur_dist > dist) {
        dist = cur_dist;
        index = f;
      }
    }
    if (dist > eps) {
      /* iterate */
      int d0 = rdp_simplify(src, index+1, dest, eps);
      if (d0 > 0) --d0; /* remove last point, as it will be the same as the new starting one */
      /* tail call, actually */
      //return d0+rdp_simplify(src+index, src_count-index, dest+d0, eps);
      res += d0;
      src += index;
      src_count -= index;
      dest += d0;
      goto again;
    }
    dest[0] = *first;
    dest[1] = *last;
    return res+2;
  }
  for (int f = 0; f < src_count; ++f) dest[f] = src[f];
  return res+src_count;
}
