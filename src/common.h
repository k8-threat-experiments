/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _COMMON_H_
#define _COMMON_H_

#include <SDL.h>


#define K8MIN(__a,__b)  ({ typeof(__a) _a = (__a); typeof((__b))_b = (__b); _a < _b ? _a : _b; })
#define K8MAX(__a,__b)  ({ typeof(__a) _a = (__a); typeof((__b))_b = (__b); _a > _b ? _a : _b; })

/* WARNING! if __a is INT_MIN, K8ABS() will return INT_MIN! */
#define K8ABS(__a)  ({ typeof(__a) _a = (__a); _a >= 0 ? _a : -(_a); })


#define K8SIGN(__a)  ({ typeof(__a) _a = (__a); _a > 0 ? 1 : _a < 0 ? -1 : 0; })

/*TODO: clever trick here */
static inline Uint8 byteclamp (Uint32 v) { return (v > 255 ? 255 : v); }


// fuckin' preprocessor trickery
#define LAMBDA(_return_type, _body_and_args)  ({ \
  _return_type __fn_lmb__ _body_and_args \
  __fn_lmb__; \
})


#endif
