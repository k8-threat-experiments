/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _FILEIO_H_
#define _FILEIO_H_

#include <SDL.h>

#include "libios/ios.h"


extern ios_t *file_ropen (const char *fname);
extern int file_read_u16 (ios_t *fl, Uint32 *r); // <0: error; 0: ok
extern int file_read_pstr (ios_t *fl, void *dest, int slen); // <0: error; 0: ok


#endif
