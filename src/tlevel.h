/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _TLEVEL_H_
#define _TLEVEL_H_

#include <SDL.h>


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int x;
  int y;
} point_t;


typedef struct {
  int x;
  int y;
  int w;
  int h;
} rect_t;


////////////////////////////////////////////////////////////////////////////////
#define MAP_WIDTH   (20)
#define MAP_HEIGHT  (24)

#define TILE_WIDTH   (16)
#define TILE_HEIGHT  (16)

#define MONSTER_WIDTH   (12)
#define MONSTER_HEIGHT  (18)


typedef enum {
 ORDER_ELIMITANE_ALL, // eliminate all monsters
 ORDER_DESTROY_ALL,   // destroy all objects
 ORDER_TIME_LIMIT,    // time limit
 ORDER_BE_HERE,       // be here in time
 ORDER_MAX
} order_type_t;


extern const char *order_text[ORDER_MAX];


typedef enum {
  MT_NONE,
  MT_MIRMUTTAJA, // imp with fireballs
  MT_BOSS, // destroyer, man with rocket launcher on the right hand
  MT_SHADOW,
  MT_SLIMER, // slimeball, green shit
  MT_WORM, // orange worm
  MT_BUG, // woodlouse
  MT_RENEGADE, // man with missile launcher
  MT_STEELER, // fast guy with machine gun
  MT_SPIDER, // green spider
  MT_HUNTER, // white, same as slimer, with flamer
  MT_TURRET, // electrogun
  MT_VIPATTAJA, // small orange spider
  MT_BRAIN, // scorched brain, flame bomb, green monster with red claws
  MT_BUNKER, // fatal hit, blue sphere
  MT_MAX
} monster_type_t;


typedef enum {
  MAI_HAUNT,
  MAI_PATH,
  MAI_WAIT,
  MAI_NOMOVE,
  MAI_MAX
} monster_ai_type_t;


typedef struct {
  monster_type_t type;
  monster_ai_type_t ai;
  point_t pos;
  point_t path[8];
} monster_info_t;


typedef struct {
  int count;
  monster_info_t m[10];
} monster_wave_info_t;


typedef struct {
  point_t pos;
  int size; // in pixels; == 0: not used
} explosive_info_t;


typedef enum {
  DT_NONE,
  DT_OPENALL,
  DT_OPENDOOR,
  // there is no SHOOT doors in game, nor horizontal doors
  DT_SHOOTWALL,
  DT_SHOOTDOOR,
  DT_MAX
} door_type_t;


typedef struct {
  door_type_t type;
  point_t pos;
} door_info_t;


typedef struct {
  rect_t barea; // button area
  rect_t earea; // 'explode' area
} button_info_t;


////////////////////////////////////////////////////////////////////////////////
extern Uint8 map[MAP_HEIGHT][MAP_WIDTH];
extern char tilemap[9]; // asciiz
extern char lvltitle[21]; // asciiz
extern char orders[4]; // indexed by order_type_t
extern int lvltime; // time limit (seconds?)
extern monster_wave_info_t mwaves[5];
extern explosive_info_t explos[10];
extern door_info_t doors[256];
extern point_t plrpos[3];
extern point_t behere_pos;
extern button_info_t buttons[256];


////////////////////////////////////////////////////////////////////////////////
// <0: error; 0: ok

extern int load_level_tiles (void);
extern int load_custom_level (const char *fname);
extern int load_mission_level (int episode, int mission);


#endif
