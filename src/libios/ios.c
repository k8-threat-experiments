/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 * based on the public domain code by Jeff Bezanson */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#ifndef __USE_FILE_OFFSET64
# define __USE_FILE_OFFSET64
#endif

#ifndef _LARGEFILE64_SOURCE
# define _LARGEFILE64_SOURCE
#endif

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <wchar.h>

#ifdef WIN32
# include <malloc.h>
# include <io.h>
# define fileno  _fileno
#endif

#include "ios.h"


////////////////////////////////////////////////////////////////////////////////
static const uint32_t offsetsFromUTF8[6] = {
  0x00000000UL, 0x00003080UL, 0x000E2080UL,
  0x03C82080UL, 0xFA082080UL, 0x82082080UL
};


static const char trailingBytesForUTF8[256] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5
};


/* is c the start of a utf8 sequence? */
#define u8_isutf(c) (((c)&0xC0) != 0x80)


/* returns length of next utf-8 sequence */
static inline size_t u8_seqlen (const unsigned char *s) {
  return trailingBytesForUTF8[(unsigned int)(unsigned char)s[0]]+1;
}


/* reads the next utf-8 sequence out of a string, updating an index */
static uint32_t u8_nextchar (const unsigned char *s, size_t *i) {
  uint32_t ch = 0;
  size_t sz = 0;
  do {
    ch <<= 6;
    ch += (unsigned char)s[(*i)];
    ++sz;
  } while (s[*i] && (++(*i)) && !u8_isutf(s[*i]));
  ch -= offsetsFromUTF8[sz-1];
  return ch;
}


/* srcsz = number of source characters
   sz = size of dest buffer in bytes
   returns # bytes stored in dest
   the destination string will never be bigger than the source string.
*/
static size_t u8_toutf8 (char *dest, size_t sz, const uint32_t *src, size_t srcsz) {
  uint32_t ch;
  size_t i = 0;
  char *dest0 = dest;
  char *dest_end = dest+sz;
  while (i < srcsz) {
    ch = src[i];
    if (ch < 0x80) {
      if (dest >= dest_end) break;
      *dest++ = (char)ch;
    } else if (ch < 0x800) {
      if (dest >= dest_end-1) break;
      *dest++ = (ch>>6)|0xC0;
      *dest++ = (ch&0x3F)|0x80;
    } else if (ch < 0x10000) {
      if (dest >= dest_end-2) break;
      *dest++ = (ch>>12)|0xE0;
      *dest++ = ((ch>>6)&0x3F)|0x80;
      *dest++ = (ch&0x3F)|0x80;
    } else if (ch < 0x110000) {
      if (dest >= dest_end-3) break;
      *dest++ = (ch>>18)|0xF0;
      *dest++ = ((ch>>12)&0x3F)|0x80;
      *dest++ = ((ch>>6)&0x3F)|0x80;
      *dest++ = (ch&0x3F)|0x80;
    }
    ++i;
  }
  return (dest-dest0);
}


////////////////////////////////////////////////////////////////////////////////
static void sleep_ms (int ms) {
  if (ms > 0) {
    struct timespec ts;
    ts.tv_sec = ms/1000;
    ts.tv_nsec = (ms%1000)*1000000;
    nanosleep(&ts, NULL);
  }
}


////////////////////////////////////////////////////////////////////////////////
#define MOST_OF(x) ((x)-((x)>>4))


////////////////////////////////////////////////////////////////////////////////
/* OS-level primitive wrappers */

// ms
#define SLEEP_TIME  (5)


static inline int _enonfatal (int err) {
  return (err == EAGAIN || err == EINPROGRESS || err == EINTR || err == EWOULDBLOCK);
}


// return error code, #bytes read in *nread
// these wrappers retry operations until success or a fatal error
static int _os_read (long fd, void *buf, size_t n, size_t *nread) {
  for (;;) {
    ssize_t r = read((int)fd, buf, n);
    if (r > -1) {
      *nread = (size_t) r;
      return 0;
    }
    if (!_enonfatal(errno)) {
      *nread = 0;
      return errno;
    }
    sleep_ms(SLEEP_TIME);
  }
  return 0;
}


static int _os_read_all (long fd, void *buf, size_t n, size_t *nread) {
  *nread = 0;
  while (n > 0) {
    size_t got;
    int err = _os_read(fd, buf, n, &got);
    n -= got;
    *nread += got;
    buf += got;
    if (err || got == 0) return err;
  }
  return 0;
}


static int _os_write (long fd, const void *buf, size_t n, size_t *nwritten) {
  for (;;) {
    ssize_t r = write((int)fd, buf, n);
    if (r > -1) {
      *nwritten = (size_t) r;
      return 0;
    }
    if (!_enonfatal(errno)) {
      *nwritten = 0;
      return errno;
    }
    sleep_ms(SLEEP_TIME);
  }
  return 0;
}


static int _os_write_all (long fd, const void *bufp, size_t n, size_t *nwritten) {
  const char *buf = (const char *)bufp;
  *nwritten = 0;
  while (n > 0) {
    size_t wrote;
    int err = _os_write(fd, buf, n, &wrote);
    n -= wrote;
    *nwritten += wrote;
    buf += wrote;
    if (err) return err;
  }
  return 0;
}


/* internal utility functions */
static char *_buf_realloc (ios_t *s, size_t sz) {
  unsigned char *temp;
  if ((s->buf == NULL || s->buf == &s->local[0]) && sz <= IOS_INLSIZE) {
    //TODO: if we want to allow shrinking, see if the buffer shrank down to this size, in which case we need to copy
    s->buf = &s->local[0];
    s->maxsize = IOS_INLSIZE;
    s->ownbuf = 1;
    return (char *)s->buf;
  }
  if (sz <= s->maxsize) return (char *)s->buf;
  if (s->ownbuf && s->buf != &s->local[0]) {
    // if we own the buffer we're free to resize it
    // always allocate 1 bigger in case user wants to add a NUL
    // terminator after taking over the buffer
    temp = realloc(s->buf, sz+1);
    if (temp == NULL) return NULL;
  } else {
    temp = malloc(sz+1);
    if (temp == NULL) return NULL;
    s->ownbuf = 1;
    if (s->size > 0) memcpy(temp, s->buf, s->size);
  }
  s->buf = temp;
  s->maxsize = sz;
  return (char *)s->buf;
}


// write a block of data into the buffer at the current position, resizing
// if necessary. returns # written.
static size_t _write_grow (ios_t *s, const void *data, size_t n) {
  size_t amt;
  size_t newsize;
  if (n == 0) return 0;
  if (s->bpos+n > s->size) {
    if (s->bpos+n > s->maxsize) {
      //TODO: here you might want to add a mechanism for limiting the growth of the stream
      newsize = (s->maxsize ? s->maxsize*2 : 8);
      while (s->bpos+n > newsize) newsize *= 2;
      if (_buf_realloc(s, newsize) == NULL) {
        /* no more space; write as much as we can */
        amt = s->maxsize-s->bpos;
        if (amt > 0) memcpy(&s->buf[s->bpos], data, amt);
        s->bpos += amt;
        s->size = s->maxsize;
        return amt;
      }
    }
    s->size = s->bpos+n;
  }
  memcpy(s->buf+s->bpos, data, n);
  s->bpos += n;
  return n;
}


/* interface functions, low level */
static size_t _ios_read (ios_t *s, void *destp, size_t n, int all) {
  size_t tot = 0;
  char *dest = (char *)destp;
  while (n > 0) {
    size_t got;
    size_t avail = s->size-s->bpos;
    if (avail > 0) {
      size_t ncopy = (avail >= n ? n : avail);
      memcpy(dest, s->buf+s->bpos, ncopy);
      s->bpos += ncopy;
      if (ncopy >= n) {
        s->state = bst_rd;
        return tot+ncopy;
      }
    }
    if (s->bm == bm_mem || s->fd == -1) {
      // can't get any more data
      s->state = bst_rd;
      if (avail == 0) s->_eof = 1;
      return avail;
    }
    dest += avail;
    n -= avail;
    tot += avail;
    ios_flush(s);
    s->bpos = s->size = 0;
    s->state = bst_rd;
    s->fpos = -1;
    if (n > MOST_OF(s->maxsize)) {
      // doesn't fit comfortably in buffer; go direct
      if (all) {
        //result = _os_read_all(s->fd, dest, n, &got);
        _os_read_all(s->fd, dest, n, &got);
      } else {
        //result = _os_read(s->fd, dest, n, &got);
        _os_read(s->fd, dest, n, &got);
      }
      tot += got;
      if (got == 0) s->_eof = 1;
      return tot;
    } else {
      // refill buffer
      if (_os_read(s->fd, s->buf, s->maxsize, &got)) {
        s->_eof = 1;
        return tot;
      }
      if (got == 0) {
        s->_eof = 1;
        return tot;
      }
      s->size = got;
    }
  }
  return tot;
}


size_t ios_read (ios_t *s, void *dest, size_t n) {
  return (s != NULL ? _ios_read(s, dest, n, 0) : 0);
}


size_t ios_readall (ios_t *s, void *dest, size_t n) {
  return (s != NULL ? _ios_read(s, dest, n, 1) : 0);
}


size_t ios_readprep (ios_t *s, size_t n) {
  size_t space, got;
  int result;
  if (s == NULL) return 0;
  if (s->state == bst_wr && s->bm != bm_mem) {
    ios_flush(s);
    s->bpos = s->size = 0;
  }
  space = s->size-s->bpos;
  s->state = bst_rd;
  if (space >= n || s->bm == bm_mem || s->fd == -1) return space;
  if (s->maxsize < s->bpos+n) {
    // it won't fit. grow buffer or move data back.
    if (n <= s->maxsize && space <= ((s->maxsize)>>2)) {
      if (space) memmove(s->buf, s->buf+s->bpos, space);
      s->size -= s->bpos;
      s->bpos = 0;
    } else {
      if (_buf_realloc(s, s->bpos+n) == NULL) return space;
    }
  }
  result = _os_read(s->fd, s->buf+s->size, s->maxsize-s->size, &got);
  if (result) return space;
  s->size += got;
  return s->size-s->bpos;
}


static void _write_update_pos (ios_t *s) {
  if (s->bpos > s->ndirty) s->ndirty = s->bpos;
  if (s->bpos > s->size) s->size = s->bpos;
}


// directly copy a buffer to a descriptor
size_t ios_write_direct (ios_t *dest, ios_t *src) {
  if (dest != NULL) {
    unsigned char *data = src->buf;
    size_t nwr;
    size_t n = src->size;
    dest->fpos = -1;
    _os_write_all(dest->fd, data, n, &nwr);
    return nwr;
  }
  return 0;
}


size_t ios_write (ios_t *s, const void *datap, size_t n) {
  if (s != NULL) {
    size_t space;
    const char *data = (const char *)datap;
    size_t wrote = 0;
    if (s->readonly) return 0;
    if (n == 0) return 0;
    if (s->state == bst_none) s->state = bst_wr;
    if (s->state == bst_rd) {
      if (!s->rereadable) {
        s->size = 0;
        s->bpos = 0;
      }
      space = s->size-s->bpos;
    } else {
      space = s->maxsize-s->bpos;
    }
    if (s->bm == bm_mem) {
      wrote = _write_grow(s, data, n);
    } else if (s->bm == bm_none) {
      s->fpos = -1;
      _os_write_all(s->fd, data, n, &wrote);
      return wrote;
    } else if (n <= space) {
      if (s->bm == bm_line) {
        char *nl;
        if ((nl = (char *)memrchr(data, '\n', n)) != NULL) {
          size_t linesz = nl-data+1;
          s->bm = bm_block;
          wrote += ios_write(s, data, linesz);
          ios_flush(s);
          s->bm = bm_line;
          n -= linesz;
          data += linesz;
        }
      }
      memcpy(s->buf+s->bpos, data, n);
      s->bpos += n;
      wrote += n;
    } else {
      s->state = bst_wr;
      ios_flush(s);
      if (n > MOST_OF(s->maxsize)) {
        _os_write_all(s->fd, data, n, &wrote);
        return wrote;
      }
      return ios_write(s, data, n);
    }
    _write_update_pos(s);
    return wrote;
  }
  return 0;
}


foff_t ios_seek (ios_t *s, foff_t pos) {
  if (s != NULL) {
    s->_eof = 0;
    if (s->bm == bm_mem) {
      if ((size_t)pos > s->size) return -1;
      s->bpos = pos;
    } else {
      ios_flush(s);
      off64_t fdpos = lseek64(s->fd, pos, SEEK_SET);
      if (fdpos == (off64_t)-1) return fdpos;
      s->bpos = s->size = 0;
    }
  }
  return 0;
}


foff_t ios_seek_end (ios_t *s) {
  if (s != NULL) {
    s->_eof = 1;
    if (s->bm == bm_mem) {
      s->bpos = s->size;
    } else {
      ios_flush(s);
      off64_t fdpos = lseek64(s->fd, 0, SEEK_END);
      if (fdpos == (off64_t)-1) return fdpos;
      s->bpos = s->size = 0;
    }
  }
  return 0;
}


foff_t ios_skip (ios_t *s, foff_t offs) {
  if (s != NULL) {
    if (offs != 0) {
      if (offs > 0) {
        if (offs <= (off64_t) (s->size-s->bpos)) {
          s->bpos += offs;
          return 0;
        } else if (s->bm == bm_mem) {
          //TODO: maybe grow buffer
          return -1;
        }
      } else if (offs < 0) {
        if (-offs <= (off64_t) s->bpos) {
          s->bpos += offs;
          s->_eof = 0;
          return 0;
        } else if (s->bm == bm_mem) {
          return -1;
        }
      }
      ios_flush(s);
      if (s->state == bst_wr) offs += s->bpos;
      else if (s->state == bst_rd) offs -= (s->size-s->bpos);
      off64_t fdpos = lseek64(s->fd, offs, SEEK_CUR);
      if (fdpos == (off64_t)-1) return fdpos;
      s->bpos = s->size = 0;
      s->_eof = 0;
    }
  }
  return 0;
}


foff_t ios_pos (ios_t *s) {
  if (s != NULL) {
    off64_t fdpos;
    if (s->bm == bm_mem) return (off64_t) s->bpos;
    fdpos = s->fpos;
    if (fdpos == (off64_t)-1) {
      fdpos = lseek64(s->fd, 0, SEEK_CUR);
      if (fdpos == (off64_t)-1) return fdpos;
      s->fpos = fdpos;
    }
    if (s->state == bst_wr) fdpos += s->bpos;
    else if (s->state == bst_rd) fdpos -= (s->size-s->bpos);
    return fdpos;
  }
  return 0;
}


size_t ios_trunc (ios_t *s, size_t size) {
  if (s != NULL) {
    if (s->bm == bm_mem) {
      if (size == s->size) return s->size;
      if (size < s->size) {
        if (s->bpos > size) s->bpos = size;
      } else {
        if (_buf_realloc(s, size) == NULL) return s->size;
      }
      s->size = size;
      return size;
    }
  }
  //TODO
  return (ssize_t)-1;
}


int ios_eof (ios_t *s) {
  if (s != NULL) {
    if (s->bm == bm_mem) return (s->_eof ? 1 : 0);
    if (s->fd == -1) return 1;
    if (s->_eof) return 1;
    return 0;
  }
  return 1;
}


int ios_flush (ios_t *s) {
  if (s != NULL) {
    int err;
    if (s->ndirty == 0 || s->bm == bm_mem || s->buf == NULL) return 0;
    if (s->fd == -1) return -1;
    if (s->state == bst_rd) {
      if (lseek64(s->fd, -(off64_t)s->size, SEEK_CUR) == (off64_t)-1) {
        //TODO: what to do here?
      }
    }
    size_t nw, ntowrite = s->ndirty;
    s->fpos = -1;
    err = _os_write_all(s->fd, s->buf, ntowrite, &nw);
    //TODO: try recovering from some kinds of errors (e.g. retry)
    if (s->state == bst_rd) {
      if (lseek64(s->fd, s->size-nw, SEEK_CUR) == (off64_t)-1) {
        //TODO: what to do here?
      }
    } else if (s->state == bst_wr) {
      if (s->bpos != nw && lseek64(s->fd, (off64_t) s->bpos-(off64_t) nw, SEEK_CUR) == (off64_t)-1) {
        //TODO: what to do here?
      }
      // now preserve the invariant that data to write
      // begins at the beginning of the buffer, and s->size refers
      // to how much valid file data is stored in the buffer.
      if (s->size > s->ndirty) {
        size_t delta = s->size-s->ndirty;
        memmove(s->buf, s->buf+s->ndirty, delta);
      }
      s->size -= s->ndirty;
      s->bpos = 0;
    }
    s->ndirty = 0;
    if (err) return err;
    if (nw < ntowrite) return -1;
  }
  return 0;
}


void ios_close (ios_t *s) {
  if (s != NULL) {
    ios_flush(s);
    if (s->fd != -1 && s->ownfd) close(s->fd);
    s->fd = -1;
    if (s->buf != NULL && s->ownbuf && s->buf != &s->local[0]) free(s->buf);
    s->buf = NULL;
    s->size = s->maxsize = s->bpos = 0;
  }
}


static void _buf_init (ios_t *s, bufmode_t bm) {
  s->bm = bm;
  if (s->bm == bm_mem || s->bm == bm_none) {
    s->buf = &s->local[0];
    s->maxsize = IOS_INLSIZE;
  } else {
    s->buf = NULL;
    _buf_realloc(s, IOS_BUFSIZE);
  }
  s->size = s->bpos = 0;
}


char *ios_takebuf (ios_t *s, size_t *psize) {
  if (s != NULL) {
    unsigned char *buf;
    ios_flush(s);
    if (s->buf == &s->local[0]) {
      buf = malloc(s->size+1);
      if (buf == NULL) return NULL;
      if (s->size) memcpy(buf, s->buf, s->size);
    } else {
      buf = s->buf;
    }
    buf[s->size] = '\0';
    if (psize != NULL) *psize = s->size+1; /* buffer is actually 1 bigger for terminating NUL */
    /* empty stream and reinitialize */
    _buf_init(s, s->bm);
    return (char *)buf;
  }
  if (psize != NULL) *psize = 0;
  return NULL;
}


int ios_setbuf (ios_t *s, void *buf, size_t size, int own) {
  if (s != NULL) {
    ios_flush(s);
    size_t nvalid = 0;
    nvalid = (size < s->size ? size : s->size);
    if (nvalid > 0) memcpy(buf, s->buf, nvalid);
    if (s->bpos > nvalid) {
      // truncated
      s->bpos = nvalid;
    }
    s->size = nvalid;
    if (s->buf != NULL && s->ownbuf && s->buf != &s->local[0]) free(s->buf);
    s->buf = buf;
    s->maxsize = size;
    s->ownbuf = own;
    return 0;
  }
  return -1;
}


int ios_bufmode (ios_t *s, bufmode_t mode) {
  if (s != NULL) {
    // no fd; can only do mem-only buffering
    if (s->fd == -1 && mode != bm_mem) return -1;
    s->bm = mode;
    return 0;
  }
  return -1;
}


void ios_set_readonly (ios_t *s) {
  if (s == NULL || s->readonly) return;
  ios_flush(s);
  s->state = bst_none;
  s->readonly = 1;
}


static size_t ios_copy_ (ios_t *to, ios_t *from, size_t nbytes, int all) {
  size_t total = 0;
  if (!ios_eof(from)) {
    size_t avail, written, ntowrite;
    do {
      avail = ios_readprep(from, IOS_BUFSIZE/2);
      if (avail == 0) {
        from->_eof = 1;
        break;
      }
      ntowrite = (avail <= nbytes || all) ? avail : nbytes;
      written = ios_write(to, from->buf+from->bpos, ntowrite);
      //TODO: should this be +=written instead?
      from->bpos += ntowrite;
      total += written;
      if (!all) {
        nbytes -= written;
        if (nbytes == 0) break;
      }
      if (written < ntowrite) break;
    } while (!ios_eof(from));
  }
  return total;
}


size_t ios_copy (ios_t *to, ios_t *from, size_t nbytes) {
  return (to != NULL && from != NULL ? ios_copy_(to, from, nbytes, 0) : 0);
}


size_t ios_copyall (ios_t *to, ios_t *from) {
  return (to != NULL && from != NULL ? ios_copy_(to, from, 0, 1) : 0);
}


#define LINE_CHUNK_SIZE  (160)

size_t ios_copyuntil (ios_t *to, ios_t *from, char delim) {
  if (to != NULL && from != NULL) {
    size_t total = 0, avail = from->size-from->bpos;
    int first = 1;
    if (!ios_eof(from)) {
      size_t written;
      do {
        if (avail == 0) {
          first = 0;
          avail = ios_readprep(from, LINE_CHUNK_SIZE);
        }
        unsigned char *pd = (unsigned char *)memchr(from->buf+from->bpos, delim, avail);
        if (pd == NULL) {
          written = ios_write(to, from->buf+from->bpos, avail);
          from->bpos += avail;
          total += written;
          avail = 0;
        } else {
          size_t ntowrite = pd-(from->buf+from->bpos)+1;
          written = ios_write(to, from->buf+from->bpos, ntowrite);
          from->bpos += ntowrite;
          total += written;
          return total;
        }
      } while (!ios_eof(from) && (first || avail >= LINE_CHUNK_SIZE));
    }
    from->_eof = 1;
    return total;
  }
  return 0;
}


static void _ios_init (ios_t *s) {
  // put all fields in a sane initial state
  s->bm = bm_block;
  s->state = bst_none;
  s->errcode = 0;
  s->buf = NULL;
  s->maxsize = 0;
  s->size = 0;
  s->bpos = 0;
  s->ndirty = 0;
  s->fpos = -1;
  s->lineno = 1;
  s->fd = -1;
  s->ownbuf = 1;
  s->ownfd = 0;
  s->_eof = 0;
  s->rereadable = 0;
  s->readonly = 0;
}


/* stream object initializers. we do no allocation. */
ios_t *ios_file (ios_t *s, const char *fname, const char *mode) {
  int fd, rd = 0, wr = 0, create = 0, trunc = 0;
  void *ptr = NULL;
  if (fname == NULL || !fname[0] || mode == NULL || !mode[0]) goto open_file_err;
  for (; *mode; ++mode) {
    switch (*mode) {
      case 'r': case 'R': rd = 1; break;
      case 'w': case 'W': wr = 1; break;
      case 'c': case 'C': create = 1; break;
      case 't': case 'T': trunc = 1; break;
    }
  }
  if (!(rd || wr)) goto open_file_err; // must specify read and/or write
  int flags = (wr ? (rd ? O_RDWR : O_WRONLY) : O_RDONLY);
  if (create) flags |= O_CREAT;
  if (trunc) flags |= O_TRUNC;
  fd = open(fname, flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH/*644*/);
  if (fd == -1) goto open_file_err;
  if (s == NULL && (s = ptr = malloc(sizeof(ios_t))) == NULL) { close(fd); goto open_file_err; }
  s = ios_fd(s, fd, 1, 1);
  if (!wr) s->readonly = 1;
  return s;
open_file_err:
  if (s != NULL) s->fd = -1;
  if (ptr != NULL) free(ptr);
  return NULL;
}


ios_t *ios_mem (ios_t *s, size_t initsize) {
  if (s == NULL && (s = malloc(sizeof(ios_t))) == NULL) return NULL;
  _ios_init(s);
  s->bm = bm_mem;
  _buf_realloc(s, initsize);
  return s;
}


ios_t *ios_str (ios_t *s, const void *str) {
  void *ptr = NULL;
  if (s == NULL && (s = ptr = malloc(sizeof(ios_t))) == NULL) return NULL;
  size_t n = (str != NULL ? strlen((const char *)str) : 0);
  if (ios_mem(s, n+1) == NULL) { if (ptr) free(ptr); return NULL; }
  if (str != NULL) {
    ios_write(s, str, n+1);
  } else {
    char ch = 0;
    ios_write(s, &ch, 1);
  }
  ios_seek(s, 0);
  return s;
}


ios_t *ios_static_buffer (ios_t *s, const void *buf, size_t sz) {
  if (s == NULL && (s = malloc(sizeof(ios_t))) == NULL) return NULL;
  ios_mem(s, 0);
  ios_setbuf(s, (void *)buf, sz, 0);
  s->size = sz;
  ios_set_readonly(s);
  return s;
}


ios_t *ios_fd (ios_t *s, long fd, int isfile, int own) {
  if (s == NULL && (s = malloc(sizeof(ios_t))) == NULL) return NULL;
  _ios_init(s);
  s->fd = fd;
  if (isfile) s->rereadable = 1;
  _buf_init(s, bm_block);
  s->ownfd = own;
  if (fd == STDERR_FILENO) s->bm = bm_none;
  return s;
}


////////////////////////////////////////////////////////////////////////////////
static int ios_std_streams_initialized = 0;
ios_t *ios_stdin = NULL;
ios_t *ios_stdout = NULL;
ios_t *ios_stderr = NULL;


/* the very first thing to do after*/
void ios_init_stdstreams (void) {
  if (!ios_std_streams_initialized) {
    ios_std_streams_initialized = 1;

    ios_stdin = malloc(sizeof(ios_t));
    ios_fd(ios_stdin, STDIN_FILENO, 0, 0);

    ios_stdout = malloc(sizeof(ios_t));
    ios_fd(ios_stdout, STDOUT_FILENO, 0, 0);
    ios_stdout->bm = bm_line;

    ios_stderr = malloc(sizeof(ios_t));
    ios_fd(ios_stderr, STDERR_FILENO, 0, 0);
    ios_stderr->bm = bm_none;
  }
}


static __attribute__((constructor(101))) void _ctor_ios (void) {
  ios_init_stdstreams();
}


////////////////////////////////////////////////////////////////////////////////
/* higher level interface */
int ios_putc (int c, ios_t *s) {
  if (s != NULL) {
    unsigned char ch = (c&0xff);
    if (s->state == bst_wr && s->bpos < s->maxsize && s->bm != bm_none) {
      s->buf[s->bpos++] = ch;
      _write_update_pos(s);
      if (s->bm == bm_line && ch == '\n') ios_flush(s);
      return 1;
    }
    return (int)ios_write(s, &ch, 1);
  }
  return 0;
}


int ios_getc (ios_t *s) {
  if (s != NULL) {
    unsigned char ch;
    if (s->state == bst_rd && s->bpos < s->size) {
      ch = s->buf[s->bpos++];
    } else {
      if (s->_eof) return IOS_EOF;
      if (ios_read(s, &ch, 1) < 1) return IOS_EOF;
    }
    if (ch == '\n') s->lineno++;
    return ch;
  }
  return IOS_EOF;
}


int ios_peekc (ios_t *s) {
  if (s != NULL) {
    size_t n;
    if (s->bpos < s->size) return s->buf[s->bpos];
    if (s->_eof) return IOS_EOF;
    n = ios_readprep(s, 1);
    if (n == 0) return IOS_EOF;
    return s->buf[s->bpos];
  }
  return 0;
}


int ios_ungetc (int c, ios_t *s) {
  if (s != NULL) {
    if (s->state == bst_wr) return IOS_EOF;
    if (s->bpos > 0) {
      --s->bpos;
      s->buf[s->bpos] = (c&0xff);
      s->_eof = 0;
      return c;
    }
    if (s->size == s->maxsize) {
      if (_buf_realloc(s, s->maxsize*2) == NULL) return IOS_EOF;
    }
    memmove(s->buf+1, s->buf, s->size);
    s->buf[0] = (c&0xff);
    ++s->size;
    s->_eof = 0;
    return c;
  }
  return IOS_EOF;
}


int ios_getutf8 (ios_t *s, uint32_t *pwc) {
  if (s != NULL) {
    int c;
    size_t sz;
    unsigned char c0;
    unsigned char buf[8];
    c = ios_getc(s);
    if (c == IOS_EOF) return IOS_EOF;
    c0 = (c&0xff);
    if ((unsigned char)c0 < 0x80) {
      *pwc = (uint32_t)(unsigned char)c0;
      return 1;
    }
    sz = u8_seqlen(&c0)-1;
    if (ios_ungetc(c, s) == IOS_EOF) return IOS_EOF;
    if (ios_readprep(s, sz) < sz) return IOS_EOF; // NOTE: this can return EOF even if some bytes are available
    size_t i = s->bpos;
    *pwc = u8_nextchar(s->buf, &i);
    ios_read(s, buf, sz+1);
    return 1;
  }
  return IOS_EOF;
}


int ios_peekutf8 (ios_t *s, uint32_t *pwc) {
  if (s != NULL) {
    int c;
    size_t sz;
    unsigned char c0;
    c = ios_peekc(s);
    if (c == IOS_EOF) return IOS_EOF;
    c0 = (c&0xff);
    if ((unsigned char)c0 < 0x80) {
      *pwc = (uint32_t) (unsigned char)c0;
      return 1;
    }
    sz = u8_seqlen(&c0)-1;
    if (ios_readprep(s, sz) < sz) return IOS_EOF;
    size_t i = s->bpos;
    *pwc = u8_nextchar(s->buf, &i);
    return 1;
  }
  return IOS_EOF;
}


int ios_pututf8 (ios_t *s, uint32_t wc) {
  if (s != NULL) {
    char buf[8];
    size_t n;
    if (wc < 0x80) return ios_putc((int)wc, s);
    n = u8_toutf8(buf, 8, &wc, 1);
    return ios_write(s, buf, n);
  }
  return 0;
}


void ios_purge (ios_t *s) {
  if (s != NULL && s->state == bst_rd) s->bpos = s->size;
}


char *ios_readline (ios_t *s) {
  if (s != NULL) {
    size_t n;
    ios_t dest;
    ios_mem(&dest, 0);
    ios_copyuntil(&dest, s, '\n');
    return ios_takebuf(&dest, &n);
  }
  return NULL;
}


int ios_vprintf (ios_t *s, const char *format, va_list args) {
  if (s != NULL) {
    int c;
    char *str = NULL;
    va_list al;
    va_copy(al, args);
    if (s->state == bst_wr && s->bpos < s->maxsize && s->bm != bm_none) {
      size_t avail = s->maxsize-s->bpos;
      unsigned char *start = s->buf+s->bpos;
      c = vsnprintf((char *)start, avail, format, args);
      if (c < 0) {
        va_end(al);
        return c;
      }
      if (c < avail) {
        s->bpos += (size_t) c;
        _write_update_pos(s);
        //TODO: only works right if newline is at end
        if (s->bm == bm_line && memrchr(start, '\n', (size_t) c))
          ios_flush(s);
        va_end(al);
        return c;
      }
    }
    c = vasprintf(&str, format, al);
    if (c >= 0) {
      ios_write(s, str, c);
      free(str);
    }
    va_end(al);
    return c;
  }
  return 0;
}


__attribute__((format(printf,2,3))) int ios_printf(ios_t *s, const char *format, ...) {
  va_list args;
  int c;
  va_start(args, format);
  c = ios_vprintf(s, format, args);
  va_end(args);
  return c;
}
