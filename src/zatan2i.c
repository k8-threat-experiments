/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "zatan2i.h"

#include <math.h>


////////////////////////////////////////////////////////////////////////////////
#define TBS  (1662)
#define GRS  (29)

//static int sin_table[360];
static uint8_t atan_table[TBS];


static __attribute__((constructor)) void _ctor_atan_tables (void) {
  //for (int f = 0; f < 360; ++f) sin_table[f] = round(65536.0*sin(M_PI*f/180.0));
  for (int f = 0; f < TBS; ++f) atan_table[f] = round(atan(f/(double)GRS)/M_PI*180.0)-45.0;
}


int32_t atan2i (int32_t dy, int32_t dx) {
  int32_t a;
  if (dy == 0) return (dx > 0 ? 0 : 180);
  if (dx == 0) return (dy > 0 ? 90 : 270);
  if (dx > 0) {
    if (dy > 0) {
      if (dx > dy) {
        a = (int64_t)dx*GRS/dy;
        return (a >= TBS ? 0 : 45-atan_table[a]);
      } else {
        a = (int64_t)dy*GRS/dx;
        return (a >= TBS ? 90 : 45+atan_table[a]);
      }
    } else {
      dy = -dy;
      if (dx > dy) {
        a = (int64_t)dx*GRS/dy;
        return (a >= TBS ? 0 : 315+atan_table[a]);
      } else {
        a = (int64_t)dy*GRS/dx;
        return (a >= TBS ? 260 : 315-atan_table[a]);
      }
    }
  } else {
    dx = -dx;
    if (dy > 0) {
      if (dx > dy) {
        a = (int64_t)dx*GRS/dy;
        return (a >= TBS ? 135+45 : 135+atan_table[a]);
      } else {
        a = (int64_t)dy*GRS/dx;
        return (a >= TBS ? 135-45 : 135-atan_table[a]);
      }
    } else {
      dy = -dy;
      if (dx > dy) {
        a = (int64_t)dx*GRS/dy;
        return (a >= TBS ? 225-45 : 225-atan_table[a]);
      } else {
        a = (int64_t)dy*GRS/dx;
        return (a >= TBS ? 225+45 : 225+atan_table[a]);
      }
    }
  }
}
