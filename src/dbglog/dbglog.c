/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
/* debug logs */
#include "dbglog.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/types.h>


////////////////////////////////////////////////////////////////////////////////
static char dbg_locker = 0;

void dbglog_lock (void) {
  while (__sync_bool_compare_and_swap(&dbg_locker, 0, 1)) usleep(1);
}


void dbglog_unlock (void) {
  /*
  __sync_synchronize();
  dbg_locker = 0;
  */
  __sync_fetch_and_and(&dbg_locker, 0);
}


////////////////////////////////////////////////////////////////////////////////
#ifndef NO_DEBUG_LOG
static pid_t mypid;
static char log_file_name[8192] = {0};
static char bin_path[8192];
static int flag_dofile = 1; /* default: yes */
static int flag_dostderr = 0;
static FILE *logfile = NULL;
static int logfile_err = 0;


static __attribute__((destructor)) void _dtor_dbglog (void) {
  dbglog_lock();
  if (logfile != NULL) {
    fclose(logfile);
    logfile = NULL;
  }
  dbglog_unlock();
}


static __attribute__((constructor)) void (void) {
  mypid = getpid();
  bin_path[sizeof(bin_path)-1] = 0;
  if (readlink("/proc/self/exe", bin_path, sizeof(bin_path)-1) < 0) {
    strcpy(bin_path, "./");
  } else {
    char *p = strrchr(bin_path, '/');
    if (p == NULL) strcpy(bin_path, "./"); else p[1] = '\0';
  }
  if (bin_path[strlen(bin_path)-1] != '/') strcat(bin_path, "/"); // just in case, hehe
  if (!log_file_name[0]) dbglog_set_filename("debug.log");
}


void dbglog_set_filename (const char *fname) {
  dbglog_lock();
  if (fname != NULL && fname[0] && strlen(fname)) {
    if (fname[0] != '/' && !(fname[0] == '.' && fname[1] == '/') && !(fname[0] == '.' && fname[1] == '.' && fname[2] == '/')) {
      snprintf(log_file_name, sizeof(log_file_name), "%s%s", bin_path, fname);
    } else {
      snprintf(log_file_name, sizeof(log_file_name), "%s", fname);
    }
  } else {
    log_file_name[0] = '\0';
  }
  if (logfile != NULL) {
    fclose(logfile);
    logfile = NULL;
  }
  logfile_err = (log_file_name[0] != 0);
  dbglog_unlock();
}


int dbglog_set_fileout (int doIt) {
  int old;
  dbglog_lock();
  old = flag_dofile;
  flag_dofile = !!doIt;
  dbglog_unlock();
  return old;
}


int dbglog_set_stderr (int doIt) {
  int old;
  dbglog_lock();
  old = flag_dostderr;
  flag_dostderr = !!doIt;
  dbglog_unlock();
  return old;
}


void dlogf (const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  dlogfVA(fmt, ap);
  va_end(ap);
}


static void write_to_file (FILE *fl, const char *pfx, const char *buf) {
  int pfxlen = strlen(pfx);
  while (*buf) {
    const char *eol;
    // first write prefix
    if (pfxlen > 0) fwrite(pfx, pfxlen, 1, fl);
    // now look for EOL
    if ((eol = strpbrk(buf, "\r\n")) == NULL) eol = buf+strlen(buf);
    // write string part
    if (eol > buf) fwrite(buf, eol-buf, 1, fl);
    // write EOL
    fputc('\n', fl);
    // skip EOL
    switch ((buf = eol)[0]) {
      case '\0': break; // done with buf
      case '\r':
        if (*(++buf) != '\n') break;
        // fallthru
      default: ++buf; break;
    }
  }
}


void dlogfVA (const char *fmt, va_list inap) {
  char timebuf[128];
  char buf[512], *dbuf = buf;
  time_t t;
  int buf_sz;
  struct tm bt;
  va_list ap;

  if (fmt == NULL) return;
  dbglog_lock();
  if (!flag_dofile && !flag_dostderr) goto quit;

  t = time(NULL);
  localtime_r(&t, &bt);
  sprintf(timebuf, "[%05i] %04i/%02i/%02i %02i:%02i:%02i: ",
    mypid,
    bt.tm_year+1900, bt.tm_mon+1, bt.tm_mday,
    bt.tm_hour, bt.tm_min, bt.tm_sec
  );

  memset(buf, 0, sizeof(buf));
  va_copy(ap, inap);
  if ((buf_sz = vsnprintf(buf, sizeof(buf), fmt, ap)) >= sizeof(buf)) {
    va_end(ap);
    if ((dbuf = malloc(buf_sz+1)) == NULL) goto quit; // alas, out of memory
    va_copy(ap, inap);
    vsnprintf(dbuf, buf_sz+1, fmt, ap);
  }
  va_end(ap);

  if (flag_dofile && logfile == NULL && logfile_err == 0 && log_file_name[0]) {
    // try to open debug log file
    logfile = fopen(log_file_name, "a");
    ++logfile_err; // don't do this again
  }

  if (flag_dofile && logfile != NULL) {
    write_to_file(logfile, timebuf, dbuf);
    fflush(logfile);
  }

  if (flag_dostderr) {
    fputs("\r\x1b[K", stderr);
    write_to_file(stderr, timebuf+13, dbuf); // skip PID and year
  }

  if (dbuf != buf) free(dbuf);

quit:
  dbglog_unlock();
}

#else

void dbglog_set_filename (const char *fname) {}
int dbglog_set_stderr (int doIt) { return 0; }
int dbglog_set_fileout (int doIt) { return 0; }

#endif
