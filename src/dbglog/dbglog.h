/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
/* debug logs */
#ifndef _DBGLOG_H_
#define _DBGLOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NO_DEBUG_LOG
# include <features.h>
# if !defined(__GLIBC__) || __GLIBC__ < 2 || !defined(__GLIBC_MINOR__) || __GLIBC_MINOR__ < 1
#  error You need at least glibc 2.1 to use debug logging!
# endif
#endif

#include <stdarg.h>


//#define NO_DEBUG_LOG

/* defaults: write to file; don't write to stderr */

extern void dbglog_lock (void);
extern void dbglog_unlock (void);
extern void dbglog_set_filename (const char *fname);
extern int dbglog_set_stderr (int doIt); /* returns previous value */
extern int dbglog_set_fileout (int doIt); /* returns previous value */
#ifndef NO_DEBUG_LOG
extern void dlogf (const char *fmt, ...) __attribute__((format(printf, 1, 2)));
extern void dlogfVA (const char *fmt, va_list ap);
#else
# define dlogf(fmt,...)   ((void)0)
# define dlogfVA(fmt,ap)  ((void)0)
#endif


#ifdef __cplusplus
}
#endif
#endif
