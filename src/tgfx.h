/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _TGFXL_H_
#define _TGFXL_H_

#include <SDL.h>

#include "videolib/videolib.h"
#include "tlevel.h"


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int sprno; // number of first tile in 'gfxmonsters'
  int walkanimcount;
  int shootanimcount;
  int dieanimcount; // only for shadow
} monster_anim_info_t;


extern const monster_anim_info_t mainms[MT_MAX];


////////////////////////////////////////////////////////////////////////////////
extern Uint32 palette[256];
extern Uint8 font[256][8];
extern Uint8 gfxmonsters[200][320];
extern Uint8 gfxother[200][320];
extern Uint8 gfxicons[200][320];
extern Uint8 tilegfx[200][320];


/* 320x384 */
#define MAP_RES_X  (MAP_WIDTH*TILE_WIDTH)
#define MAP_RES_Y  (MAP_HEIGHT*TILE_HEIGHT)


extern Uint8 mapbmp_orig[MAP_RES_Y][MAP_RES_X]; /* original map bitmap */
extern Uint8 mapbmp[MAP_RES_Y][MAP_RES_X]; /* working map bitmap, with blitted monsters, etc; will be processed by light processor */
extern Uint8 monbmp[MAP_RES_Y][MAP_RES_X]; /* monster bitmap; contains monster number in the current wave */


////////////////////////////////////////////////////////////////////////////////
extern void filter_mapbmp (void);
extern void map_detect_lights (void);


////////////////////////////////////////////////////////////////////////////////
extern int load_font (const char *fname); // <0: error; 0: ok
// gfxbuf should be 64000 bytes long
extern int load_tpcx (void *gfxbuf, const char *fname); // <0: error; 0: ok


////////////////////////////////////////////////////////////////////////////////
extern void tr_draw_char (int x, int y, char ch, Uint32 clr);
extern void tr_draw_str (int x, int y, const char *str, Uint32 clr);
extern void tr_draw_pos (const point_t *pos, Uint32 c);

extern void tr_draw_tile_on_mapbmp_orig (int sx, int sy, Uint8 t);
extern void tr_draw_map_on_mapbmp_orig (void);

extern void tr_draw_sprite_mono (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp, Uint32 clr);
extern void tr_draw_sprite (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp);
extern void tr_draw_sprite_on_map (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp);
extern void tr_draw_sprite_on_mon (int sx, int sy, Uint8 spr_idx, const Uint8 *bmp, Uint8 value);

static inline Uint8 map_get_pixel (int x, int y) { return (x >= 0 && y >= 0 && x < MAP_RES_X && y < MAP_RES_Y ? mapbmp[y][x] : 0); }
static inline Uint8 map_get_pixel_orig (int x, int y) { return (x >= 0 && y >= 0 && x < MAP_RES_X && y < MAP_RES_Y ? mapbmp_orig[y][x] : 0); }
static inline Uint8 mon_get_pixel (int x, int y) { return (x >= 0 && y >= 0 && x < MAP_RES_X && y < MAP_RES_Y ? monbmp[y][x] : 0); }
static inline void map_put_pixel (int x, int y, Uint8 c) { if (x >= 0 && y >= 0 && x < MAP_RES_X && y < MAP_RES_Y) mapbmp[y][x] = c; }
static inline void map_put_pixel_orig (int x, int y, Uint8 c) { if (x >= 0 && y >= 0 && x < MAP_RES_X && y < MAP_RES_Y) mapbmp_orig[y][x] = c; }
static inline void mon_put_pixel (int x, int y, Uint8 c) { if (x >= 0 && y >= 0 && x < MAP_RES_X && y < MAP_RES_Y) monbmp[y][x] = c; }

static inline void tr_draw_monster (int sx, int sy, Uint8 spr_idx) { tr_draw_sprite(sx, sy, spr_idx, (void *)gfxmonsters); }
static inline void tr_draw_monster_on_map (int sx, int sy, Uint8 spr_idx) { tr_draw_sprite_on_map(sx, sy, spr_idx, (void *)gfxmonsters); }
static inline void tr_draw_monster_on_mon (int sx, int sy, Uint8 spr_idx, Uint8 value) { tr_draw_sprite_on_mon(sx, sy, spr_idx, (void *)gfxmonsters, value); }
static inline void tr_draw_otherspr (int sx, int sy, Uint8 spr_idx) { tr_draw_sprite(sx, sy, spr_idx, (void *)gfxother); }
static inline void tr_draw_otherspr_mono (int sx, int sy, Uint8 spr_idx, Uint32 clr) { tr_draw_sprite_mono(sx, sy, spr_idx, (void *)gfxother, clr); }
static inline void tr_draw_otherspr_on_map (int sx, int sy, Uint8 spr_idx) { tr_draw_sprite_on_map(sx, sy, spr_idx, (void *)gfxother); }


#endif
