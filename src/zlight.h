/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _ZLIGHT_H_
#define _ZLIGHT_H_

#include <SDL.h>


////////////////////////////////////////////////////////////////////////////////
extern int light_dump;
extern int player_light; /* is this player light? player light lights monsters */


////////////////////////////////////////////////////////////////////////////////
extern void (*lb_monster_hit_by_light) (int mnum, int x, int y, int alpha);


////////////////////////////////////////////////////////////////////////////////
extern void render_warfog_sphere (int cx, int cy, int radius);
extern void renderlight_sector (int x, int y, int radius, int as, int ae, Uint32 rgba);

extern void lb_reset (void);
extern void lb_draw (void);

extern void reset_level_lights (void);
extern void reset_lights (void);
extern void fix_level_lights (void);
extern void render_lights (void);

extern int add_light_sector_mr (int x, int y, int radius, int as, int ae, Uint32 rgba, int minrad);
extern int add_light_sector (int x, int y, int radius, int as, int ae, Uint32 rgba);


#endif
