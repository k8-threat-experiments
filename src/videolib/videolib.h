/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#ifndef _VIDEOLIB_H_
#define _VIDEOLIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <SDL.h>


////////////////////////////////////////////////////////////////////////////////
//#define VIDEOLIB_ENABLE_SFONT
//#define VIDEOLIB_ENABLE_READ_STR
//#define VIDEOLIB_ENABLE_CIRCLE_ELLIPSE
//#define VIDEOLIB_ENABLE_TEXT
//#define VIDEOLIB_ENABLE_PRTEXT
//#define VIDEOLIB_ENABLE_CLOCK


////////////////////////////////////////////////////////////////////////////////
#define TRANSPARENT_COLOR  (0xff000000u)

#define READSTR_QUIT  ((const char *)1)

#ifndef SCR_WIDTH
# define SCR_WIDTH   (320)
#endif
#ifndef SCR_HEIGHT
# define SCR_HEIGHT  (240)
#endif


////////////////////////////////////////////////////////////////////////////////
typedef enum {
  SDL_FILTER_NONE,
  SDL_FILTER_BLACKNWHITE,
  SDL_FILTER_GREEN
} sdl_filter_t;

////////////////////////////////////////////////////////////////////////////////
extern int sdl_fs; // this can be set to 1 before calling videolib_init()
extern int sdl_opengl; // this can be set to 0 before calling videolib_init()
extern int sdl_mag2x; // set to !0 to create double-sized window; default: 1; have no effect after calling videolib_init()
extern int sdl_scanlines; // set to !0 to use 'scanline' filter in mag2x mode; default: 0
extern sdl_filter_t sdl_filter; // default: SDL_FILTER_NONE

extern SDL_Window *sdl_win;
extern SDL_Renderer *sdl_rend;
extern SDL_Texture *sdl_scr;
extern Uint32 *sdl_vscr;

////////////////////////////////////////////////////////////////////////////////
extern void videolib_args (int *argc, char *argv[]);

extern int videolib_init (const char *window_name);
extern int videolib_deinit (void); /* you don't need to call this */
extern void switch_fullscreen (void);
extern void post_quit_message (void);

////////////////////////////////////////////////////////////////////////////////
static inline Uint32 rgb2col (Uint8 r, Uint8 g, Uint8 b) { return (r<<16)|(g<<8)|b; }
static inline Uint32 rgba2col (Uint8 r, Uint8 g, Uint8 b, Uint8 a) { return (a<<24)|(r<<16)|(g<<8)|b; }

static inline void put_pixel (int x, int y, Uint32 col) {
  if (x >= 0 && y >= 0 && x < SCR_WIDTH && y < SCR_HEIGHT && (col&0xff000000u) != 0xff000000u) {
    Uint32 *da = &sdl_vscr[y*SCR_WIDTH+x];
    if (col&0xff000000u) {
      const Uint32 a = 256-(col>>24); /* to not loose bits */
      const Uint32 s = col&0xffffff;
      const Uint32 dc = (*da)>>8;
      const Uint32 srb = (s&0xff00ff);
      const Uint32 sg = (s&0x00ff00);
      const Uint32 drb = (dc&0xff00ff);
      const Uint32 dg = (dc&0x00ff00);
      const Uint32 orb = (drb+(((srb-drb)*a+0x800080)>>8))&0xff00ff;
      const Uint32 og = (dg+(((sg-dg)*a+0x008000)>>8))&0x00ff00;
      *da = orb|og;
    } else {
      *da = col;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
extern int sdl_frame_changed; // this will be set to 0 by paint_screen()

extern void paint_screen (void);

////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_TEXT
extern void draw_char (int x, int y, char ch, Uint32 col, Uint32 bkcol);
extern void draw_str (int x, int y, const char *str, Uint32 col, Uint32 bkcol);
#endif

#ifdef VIDEOLIB_ENABLE_PRTEXT
extern int char_width_pr (char ch);
extern int str_width_pr (const char *str);
extern int draw_char_pr (int x, int y, char ch, Uint32 col, Uint32 bkcol);
extern int draw_str_pr (int x, int y, const char *str, Uint32 col, Uint32 bkcol);
#endif

////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_SFONT
extern int char_width_sf (char ch);
extern int str_width_sf (const char *str);
extern int draw_char_sf (char ch, int x, int y, Uint32 c0, Uint32 c1);
extern int draw_str_sf (const char *str, int x, int y, Uint32 c0, Uint32 c1);
#endif

////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_READ_STR
extern const char *read_str (void);
#endif

////////////////////////////////////////////////////////////////////////////////
extern void draw_hline (int x0, int y0, int len, Uint32 col);
extern void draw_vline (int x0, int y0, int len, Uint32 col);
extern void draw_line (int x0, int y0, int x1, int y1, Uint32 col);
/* draw line without last point */
extern void draw_line_no_last (int x0, int y0, int x1, int y1, Uint32 col);

extern void fill_rect (int x, int y, int w, int h, Uint32 col);
extern void draw_rect (int x, int y, int w, int h, Uint32 col);
/* 4 phases */
extern void draw_selection_rect (int phase, int x0, int y0, int wdt, int hgt, Uint32 col0, Uint32 col1);

#ifdef VIDEOLIB_ENABLE_CIRCLE_ELLIPSE
extern void draw_circle (int cx, int cy, int radius, Uint32 clr);
extern void draw_ellipse (int x0, int y0, int x1, int y1, Uint32 clr);
extern void draw_filled_circle (int cx, int cy, int radius, Uint32 clr);
extern void draw_filled_ellipse (int x0, int y0, int x1, int y1, Uint32 clr);
#endif


////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_CLOCK
/* returns monitonically increasing time; starting value is UNDEFINED (i.e. can be any number) */
extern Uint64 clock_milli (void); // milliseconds; (0: no timer available)
extern Uint64 clock_micro (void); // microseconds; (0: no timer available)
#endif


#ifdef __cplusplus
}
#endif
#endif
