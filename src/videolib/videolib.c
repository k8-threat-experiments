/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#include "videolib.h"


////////////////////////////////////////////////////////////////////////////////
static int sdl_size_set = 0;

int sdl_fs = 0;
int sdl_opengl = 1;

int sdl_mag2x = 1;
int sdl_scanlines = 0;
sdl_filter_t sdl_filter = SDL_FILTER_NONE;

static int sdlc_mag2x; /* effective sdl_mag2x */
static int sdl_inited = 0;


////////////////////////////////////////////////////////////////////////////////
SDL_Window *sdl_win = NULL;
SDL_Renderer *sdl_rend = NULL;
SDL_Texture *sdl_scr = NULL;
Uint32 *sdl_vscr = NULL;
static SDL_Texture *sdl_scr2x = NULL;
static Uint32 *sdl_vscr2x = NULL;
static int sdl_prev_log_size_1x = 0;


////////////////////////////////////////////////////////////////////////////////
void videolib_args (int *argc, char *argv[]) {
  int f = 1;
  while (f < argc[0]) {
    int remove = 1;
    if (strcmp(argv[f], "--") == 0) break;
    else if (strcmp(argv[f], "--fs") == 0) sdl_fs = 1;
    else if (strcmp(argv[f], "--tv") == 0) sdl_scanlines = 1;
    else if (strcmp(argv[f], "--bw") == 0) sdl_filter = SDL_FILTER_BLACKNWHITE;
    else if (strcmp(argv[f], "--green") == 0) sdl_filter = SDL_FILTER_GREEN;
    else if (strcmp(argv[f], "--1x") == 0) sdl_mag2x = 0;
    else remove = 0;
    if (remove) {
      for (int c = f+1; c < *argc; ++c) argv[c-1] = argv[c];
      --argc[0];
    } else {
      ++f;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static void cleanup (void) {
  if (sdl_scr) SDL_DestroyTexture(sdl_scr);
  if (sdl_scr2x) SDL_DestroyTexture(sdl_scr2x);
  if (sdl_rend) SDL_DestroyRenderer(sdl_rend);
  if (sdl_win) SDL_DestroyWindow(sdl_win);
  if (sdl_vscr) free(sdl_vscr);
  if (sdl_vscr2x) free(sdl_vscr2x);
  sdl_win = NULL;
  sdl_rend = NULL;
  sdl_scr = NULL;
  sdl_vscr = NULL;
  sdl_scr2x = NULL;
  sdl_vscr2x = NULL;
}


static void quit_cleanup (void) {
  cleanup();
  if (sdl_inited) { SDL_Quit(); sdl_inited = 0; }
}


int videolib_deinit (void) {
  quit_cleanup();
  return 0;
}


int videolib_init (const char *window_name) {
  if (sdl_win == NULL) {
    sdl_size_set = (sdl_fs == 0);
    SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS|SDL_INIT_TIMER);
    sdl_inited = 1;
    atexit(quit_cleanup);
    sdlc_mag2x = (!!sdl_mag2x)+1;
    sdl_prev_log_size_1x = 1;
    sdl_vscr = realloc(sdl_vscr, SCR_WIDTH*SCR_HEIGHT*sizeof(sdl_vscr[0]));
    if (sdl_vscr == NULL) {
      fprintf(stderr, "FATAL: out of memory for virtual screen!\n");
      return -1;
    }
    sdl_vscr2x = realloc(sdl_vscr2x, SCR_WIDTH*sdlc_mag2x*SCR_HEIGHT*sdlc_mag2x*sizeof(sdl_vscr2x[0]));
    if (sdl_vscr == NULL) {
      fprintf(stderr, "FATAL: out of memory for 2x virtual screen!\n");
      return -1;
    }
    sdl_win = SDL_CreateWindow((window_name ? window_name : "SDL Application"),
                SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCR_WIDTH*sdlc_mag2x, SCR_HEIGHT*sdlc_mag2x,
                (sdl_fs ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0)|(sdl_opengl ? SDL_WINDOW_OPENGL : 0));
    if (sdl_win == NULL) {
      fprintf(stderr, "FATAL: can't create SDL window: %s\n", SDL_GetError());
      return -1;
    }
    sdl_rend = SDL_CreateRenderer(sdl_win, -1, SDL_RENDERER_PRESENTVSYNC);
    if (sdl_rend == NULL) {
      fprintf(stderr, "FATAL: can't create SDL renderer: %s\n", SDL_GetError());
      return -1;
    }
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest"); // "nearest" or "linear"
    SDL_RenderSetLogicalSize(sdl_rend, SCR_WIDTH, SCR_HEIGHT);
    sdl_scr = SDL_CreateTexture(sdl_rend, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, SCR_WIDTH, SCR_HEIGHT);
    if (sdl_scr == NULL) {
      fprintf(stderr, "FATAL: can't create SDL texture: %s\n", SDL_GetError());
      return -1;
    }
    if (sdlc_mag2x == 2) {
      sdl_scr2x = SDL_CreateTexture(sdl_rend, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, SCR_WIDTH*2, SCR_HEIGHT*2);
      if (sdl_scr2x == NULL) {
        fprintf(stderr, "FATAL: can't create SDL texture: %s\n", SDL_GetError());
        return -1;
      }
    }
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
void switch_fullscreen (void) {
  sdl_fs = !sdl_fs;
  SDL_SetWindowFullscreen(sdl_win, (sdl_fs ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
  if (!sdl_fs /*&& !sdl_size_set*/) {
    sdl_size_set = 1;
    SDL_SetWindowSize(sdl_win, SCR_WIDTH*sdlc_mag2x, SCR_HEIGHT*sdlc_mag2x);
    SDL_SetWindowPosition(sdl_win, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
  }
  SDL_RenderSetLogicalSize(sdl_rend, SCR_WIDTH*(2-sdl_prev_log_size_1x), SCR_HEIGHT*(2-sdl_prev_log_size_1x));
}


////////////////////////////////////////////////////////////////////////////////
extern void post_quit_message (void) {
  SDL_Event evt;
  memset(&evt, 0, sizeof(evt));
  evt.type = SDL_QUIT;
  SDL_PushEvent(&evt);
}


////////////////////////////////////////////////////////////////////////////////
int sdl_frame_changed = 1;


////////////////////////////////////////////////////////////////////////////////
static void blit1x_bw (void) {
  const Uint8 *s = (const Uint8 *)sdl_vscr;
  Uint8 *d = (Uint8 *)sdl_vscr2x;
  int f = SCR_WIDTH*SCR_HEIGHT;
  while (f--) {
    Uint8 i = (s[0]*28+s[1]*151+s[2]*77)/256; /* intensity */
    d[0] = d[1] = d[2] = i;
    s += 4;
    d += 4;
  }
}


static void blit1x_green (void) {
  const Uint8 *s = (const Uint8 *)sdl_vscr;
  Uint8 *d = (Uint8 *)sdl_vscr2x;
  int f = SCR_WIDTH*SCR_HEIGHT;
  while (f--) {
    Uint8 i = (s[0]*28+s[1]*151+s[2]*77)/256; /* intensity */
    d[0] = d[2] = 0;
    d[1] = i;
    s += 4;
    d += 4;
  }
}


////////////////////////////////////////////////////////////////////////////////
static void blit2xtv (void) {
  const Uint32 *s = sdl_vscr;
  Uint32 *d = sdl_vscr2x;
  for (int y = 0; y < SCR_HEIGHT; ++y) {
    int x = SCR_WIDTH;
    while (x--) {
      Uint32 c0 = *s++, c1;
      c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      d[0] = d[1] = c0;
      d[SCR_WIDTH*2+0] = d[SCR_WIDTH*2+1] = c1;
      d += 2;
    }
    /* fix d: skip one scanline */
    d += SCR_WIDTH*2;
  }
}


static void blit2xtv_bw (void) {
  const Uint8 *s = (const Uint8 *)sdl_vscr;
  Uint32 *d = sdl_vscr2x;
  for (int y = 0; y < SCR_HEIGHT; ++y) {
    int x = SCR_WIDTH;
    while (x--) {
      Uint32 c0, c1;
      Uint8 i = (s[0]*28+s[1]*151+s[2]*77)/256; /* intensity */
      c0 = (i<<16)|(i<<8)|i;
      c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      d[0] = d[1] = c0;
      d[SCR_WIDTH*2+0] = d[SCR_WIDTH*2+1] = c1;
      s += 4;
      d += 2;
    }
    /* fix d: skip one scanline */
    d += SCR_WIDTH*2;
  }
}


static void blit2xtv_green (void) {
  const Uint8 *s = (const Uint8 *)sdl_vscr;
  Uint32 *d = sdl_vscr2x;
  for (int y = 0; y < SCR_HEIGHT; ++y) {
    int x = SCR_WIDTH;
    while (x--) {
      Uint32 c0, c1;
      Uint8 i = (s[0]*28+s[1]*151+s[2]*77)/256; /* intensity */
      c0 = i<<8;
      c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      d[0] = d[1] = c0;
      d[SCR_WIDTH*2+0] = d[SCR_WIDTH*2+1] = c1;
      s += 4;
      d += 2;
    }
    /* fix d: skip one scanline */
    d += SCR_WIDTH*2;
  }
}


////////////////////////////////////////////////////////////////////////////////
void paint_screen (void) {
  /* fix 'logical size' */
  if (sdlc_mag2x == 2 && sdl_scanlines) {
    /* mag2x and scanlines: size is 2x */
    if (sdl_prev_log_size_1x) {
      sdl_prev_log_size_1x = 0;
      SDL_RenderSetLogicalSize(sdl_rend, SCR_WIDTH*2, SCR_HEIGHT*2);
    }
  } else {
    /* any other case: size is 2x */
    if (!sdl_prev_log_size_1x) {
      sdl_prev_log_size_1x = 1;
      SDL_RenderSetLogicalSize(sdl_rend, SCR_WIDTH, SCR_HEIGHT);
    }
  }
  /* apply filters if any */
  if (sdlc_mag2x == 2 && sdl_scanlines) {
    /* heavy case: scanline filter turned on */
    switch (sdl_filter) {
      case SDL_FILTER_NONE: blit2xtv(); break;
      case SDL_FILTER_BLACKNWHITE: blit2xtv_bw(); break;
      case SDL_FILTER_GREEN: blit2xtv_green(); break;
    }
    SDL_UpdateTexture(sdl_scr2x, NULL, sdl_vscr2x, SCR_WIDTH*2*sizeof(sdl_vscr2x[0]));
    SDL_RenderCopy(sdl_rend, sdl_scr2x, NULL, NULL);
  } else {
    /* light cases */
    if (sdl_filter == SDL_FILTER_NONE) {
      /* easiest case */
      SDL_UpdateTexture(sdl_scr, NULL, sdl_vscr, SCR_WIDTH*sizeof(sdl_vscr[0]));
    } else {
      switch (sdl_filter) {
        case SDL_FILTER_BLACKNWHITE: blit1x_bw(); break;
        case SDL_FILTER_GREEN: blit1x_green(); break;
        default: memcpy(sdl_vscr2x, sdl_vscr, SCR_WIDTH*SCR_HEIGHT); break; /* just in case */
      }
      SDL_UpdateTexture(sdl_scr, NULL, sdl_vscr2x, SCR_WIDTH*sizeof(sdl_vscr2x[0]));
    }
    SDL_RenderCopy(sdl_rend, sdl_scr, NULL, NULL);
  }
  SDL_RenderPresent(sdl_rend);
  sdl_frame_changed = 0;
}


////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_TEXT
#include "vfont_msx.c"


void draw_char (int x, int y, char ch, Uint32 col, Uint32 bkcol) {
  int pos = (ch&0xff)*8;
  for (int dy = 0; dy < 8; ++dy) {
    unsigned char b = font_msx[pos++];
    for (int dx = 0; dx < 6; ++dx) {
      if (b&0x80) { if (col != TRANSPARENT_COLOR) put_pixel(x+dx, y+dy, col); }
      else if (bkcol != TRANSPARENT_COLOR) put_pixel(x+dx, y+dy, bkcol);
      b = (b&0x7f)<<1;
    }
  }
}


void draw_str (int x, int y, const char *str, Uint32 col, Uint32 bkcol) {
  if (!str) return;
  while (*str) {
    draw_char(x, y, *str++, col, bkcol);
    x += 6;
  }
}
#endif


////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_PRTEXT
#include "vfont_pr.c"
int char_width_pr (char ch) {
  return (prfontwdt[ch&0xff]);
}


int str_width_pr (const char *str) {
  int wdt = 0;
  if (str != NULL && str[0]) {
    while (*str) {
      wdt += prfontwdt[str[0]&0xff]+1;
      ++str;
    }
    --wdt;
  }
  return wdt;
}


int draw_char_pr (int x, int y, char ch, Uint32 col, Uint32 bkcol) {
  int pos = (ch&0xff)*8;
  int wdt = prfontwdt[ch&0xff];
  for (int dy = 0; dy < 8; ++dy) {
    unsigned char b = prfont[pos++];
    for (int dx = 0; dx < wdt; ++dx) {
      Uint32 c = (b&0x80 ? col : bkcol);
      if (c != TRANSPARENT_COLOR) put_pixel(x+dx, y+dy, c);
      b = (b&0x7f)<<1;
    }
  }
  return wdt;
}


int draw_str_pr (int x, int y, const char *str, Uint32 col, Uint32 bkcol) {
  int wdt = 0;
  if (str != NULL && str[0]) {
    while (*str) {
      wdt += prfontwdt[str[0]&0xff]+1;
      x += draw_char_pr(x, y, *str++, col, bkcol);
      if (*str && bkcol != TRANSPARENT_COLOR) for (int dy = 0; dy < 8; ++dy) put_pixel(x, y+dy, bkcol);
      ++x;
    }
    --wdt;
  }
  return wdt;
}
#endif


////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_SFONT
#include "vfont_sf.c"


int char_width_sf (char ch) {
  if (ch >= 'a' && ch <= 'z') ch -= 32;
  if (ch < 33 || ch > 95) ch = '?';
  if (ch != ' ') {
    const unsigned char *st = font_sf+(ch-32)*2;
    int ofs = st[0]+256*st[1];
    st = font_sf+ofs;
    return (*st)&0x0f;
  }
  return font_sf[1];
}


int str_width_sf (const char *str) {
  int wdt = 0;
  if (str != NULL) {
    while (*str) wdt += char_width_sf(*str++);
  }
  return wdt;
}


int draw_char_sf (char ch, int x, int y, Uint32 c0, Uint32 c1) {
  if (ch >= 'a' && ch <= 'z') ch -= 32;
  if (ch < 33 || ch > 95) ch = '?';
  if (ch != ' ') {
    const unsigned char *st = font_sf+(ch-32)*2;
    int ofs = st[0]+256*st[1], wdt, yofs, hgt = font_sf[0];
    //
    st = font_sf+ofs;
    wdt = *st++;
    yofs = ((wdt>>4)&0x0f);
    wdt &= 0x0f;
    if ((c0&0xff) == 0xff && (c1&0xff) == 0xff) return wdt;
    y += yofs;
    while (hgt-- > 0) {
      int px = x;
      for (int dx = 0; dx < wdt; dx += 4) {
        Uint8 b = *st++;
        for (int c = 0; c < 4; ++c, ++px) {
          switch ((b>>6)&0x03) {
            case 0: break;
            case 1: put_pixel(px, y, c0); break;
            case 2: put_pixel(px, y, c1); break;
            case 3: /*put_pixel(x+dx+c, y+dy, 2);*/ break;
          }
          b = ((b&0x3f)<<2);
        }
      }
      ++y;
    }
    return wdt;
  } else {
    return font_sf[1];
  }
}


int draw_str_sf (const char *str, int x, int y, Uint32 c0, Uint32 c1) {
  int wdt = 0;
  if (str != NULL) {
    while (*str) {
      int w = draw_char_sf(*str++, x, y, c0, c1);
      wdt += w;
      x += w;
    }
  }
  return wdt;
}
#endif


////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_READ_STR
const char *read_str (void) {
  static char buf[128];
  memset(buf, 0, sizeof(buf));
  SDL_StartTextInput();
  for (;;) {
    SDL_Event event;
    draw_rect(0, 0, SCR_WIDTH, 12, rgb2col(0, 0, 255));
    fill_rect(1, 1, SCR_WIDTH, 10, rgb2col(0, 0, 255));
    draw_str(2, 2, buf, rgb2col(255, 255, 0), TRANSPARENT_COLOR);
    fill_rect(2+6*strlen(buf), 2, 6, 8, rgb2col(255, 255, 255));
    paint_screen();
    if (SDL_WaitEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          SDL_StopTextInput();
          return READSTR_QUIT;
        case SDL_KEYDOWN:
          //ctrlDown = event.key.keysym.mod&KMOD_CTRL;
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
              SDL_StopTextInput();
              return NULL;
            case SDLK_BACKSPACE:
              if (strlen(buf) > 0) buf[strlen(buf)-1] = '\0';
              break;
            case SDLK_RETURN:
            case SDLK_KP_ENTER:
              SDL_StopTextInput();
              return buf;
          }
          break;
        case SDL_TEXTINPUT:
          if (!event.text.text[1] && (unsigned char)(event.text.text[0]) >= 32 && (unsigned char)(event.text.text[0]) < 127) {
            if (strlen(buf) < sizeof(buf)+1) {
              buf[strlen(buf)+1] = '\0';
              buf[strlen(buf)] = event.text.text[0];
            }
          }
          break;
      }
    }
  }
}
#endif


////////////////////////////////////////////////////////////////////////////////
void draw_hline (int x0, int y0, int len, Uint32 col) {
  while (len-- > 0) put_pixel(x0++, y0, col);
}


void draw_vline (int x0, int y0, int len, Uint32 col) {
  while (len-- > 0) put_pixel(x0, y0++, col);
}


void draw_line (int x0, int y0, int x1, int y1, Uint32 col) {
  int dx =  abs(x1-x0), sx = (x0 < x1 ? 1 : -1);
  int dy = -abs(y1-y0), sy = (y0 < y1 ? 1 : -1);
  int err = dx+dy, e2; /* error value e_xy */
  for (;;) {
    put_pixel(x0, y0, col);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


void draw_line_no_last (int x0, int y0, int x1, int y1, Uint32 col) {
  int dx =  abs(x1-x0), sx = (x0 < x1 ? 1 : -1);
  int dy = -abs(y1-y0), sy = (y0 < y1 ? 1 : -1);
  int err = dx+dy, e2; /* error value e_xy */
  for (;;) {
    if (x0 == x1 && y0 == y1) break;
    put_pixel(x0, y0, col);
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


void fill_rect (int x, int y, int w, int h, Uint32 col) {
  if (w > 0 && h > 0 && x < SCR_WIDTH && y < SCR_HEIGHT && x+w >= 0 && y+h >= 0) {
    SDL_Rect r, sr, dr;
    sr.x = 0; sr.y = 0; sr.w = SCR_WIDTH; sr.h = SCR_HEIGHT;
    r.x = x; r.y = y; r.w = w; r.h = h;
    if (SDL_IntersectRect(&sr, &r, &dr)) {
      y = dr.y;
      while (dr.h-- > 0) {
        x = dr.x;
        for (int dx = dr.w; dx > 0; --dx, ++x) put_pixel(x, y, col);
        ++y;
      }
      /*
      Uint32 a = dr.y*SCR_WIDTH+dr.x;
      while (dr.h-- > 0) {
        Uint32 da = a;
        for (int f = dr.w; f > 0; --f) sdl_vscr[da++] = col;
        a += SCR_WIDTH;
      }
      */
    }
  }
}


void draw_rect (int x, int y, int w, int h, Uint32 col) {
  if (w > 0 && h > 0) {
    draw_hline(x, y, w, col);
    draw_hline(x, y+h-1, w, col);
    draw_vline(x, y+1, h-2, col);
    draw_vline(x+w-1, y+1, h-2, col);
  }
}


void draw_selection_rect (int phase, int x0, int y0, int wdt, int hgt, Uint32 col0, Uint32 col1) {
  if (wdt > 0 && hgt > 0) {
    // top
    for (int f = x0; f < x0+wdt; ++f, ++phase) put_pixel(f, y0, ((phase %= 4) < 2 ? col0 : col1));
    // right
    for (int f = y0+1; f < y0+hgt; ++f, ++phase) put_pixel(x0+wdt-1, f, ((phase %= 4) < 2 ? col0 : col1));
    // bottom
    for (int f = x0+wdt-2; f >= x0; --f, ++phase) put_pixel(f, y0+hgt-1, ((phase %= 4) < 2 ? col0 : col1));
    // left
    for (int f = y0+hgt-2; f >= y0; --f, ++phase) put_pixel(x0, f, ((phase %= 4)<2 ? col0 : col1));
  }
}


////////////////////////////////////////////////////////////////////////////////
#ifdef VIDEOLIB_ENABLE_CIRCLE_ELLIPSE

#define PLOT_4_POINTS(_cx,_cy,_x,_y)  do { \
  put_pixel((_cx)+(_x), (_cy)+(_y), clr); \
  if ((_x) != 0) put_pixel((_cx)-(_x), (_cy)+(_y), clr); \
  if ((_y) != 0) put_pixel((_cx)+(_x), (_cy)-(_y), clr); \
  put_pixel((_cx)-(_x), (_cy)-(_y), clr); \
} while (0)


void draw_circle (int cx, int cy, int radius, Uint32 clr) {
  if (radius > 0 && (clr&0xff) != 0xff) {
    int error = -radius, x = radius, y = 0;
    if (radius == 1) { put_pixel(cx, cy, clr); return; }
    while (x > y) {
      PLOT_4_POINTS(cx, cy, x, y);
      PLOT_4_POINTS(cx, cy, y, x);
      error += y*2+1;
      ++y;
      if (error >= 0) { --x; error -= x*2; }
    }
    PLOT_4_POINTS(cx, cy, x, y);
  }
}


void draw_filled_circle (int cx, int cy, int radius, Uint32 clr) {
  if (radius > 0 && (clr&0xff) != 0xff) {
    int error = -radius, x = radius, y = 0;
    if (radius == 1) { put_pixel(cx, cy, clr); return; }
    while (x >= y) {
      int last_y = y;
      error += y;
      ++y;
      error += y;
      draw_hline(cx-x, cy+last_y, 2*x+1, clr);
      if (x != 0 && last_y != 0) draw_hline(cx-x, cy-last_y, 2*x+1, clr);
      if (error >= 0) {
        if (x != last_y) {
          draw_hline(cx-last_y, cy+x, 2*last_y+1, clr);
          if (last_y != 0 && x != 0) draw_hline(cx-last_y, cy-x, 2*last_y+1, clr);
        }
        error -= x;
        --x;
        error -= x;
      }
    }
  }
}


void draw_ellipse (int x0, int y0, int x1, int y1, Uint32 clr) {
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; /* values of diameter */
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  long err = dx+dy+b1*a*a; /* error of 1.step */
  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points... */
  if (y0 > y1) y0 = y1; /* ...exchange them */
  y0 += (b+1)/2; y1 = y0-b1;  /* starting pixel */
  a *= 8*a; b1 = 8*b*b;
  do {
    int e2;
    put_pixel(x1, y0, clr); /*   I. Quadrant */
    put_pixel(x0, y0, clr); /*  II. Quadrant */
    put_pixel(x0, y1, clr); /* III. Quadrant */
    put_pixel(x1, y1, clr); /*  IV. Quadrant */
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } /* x step */
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  /* y step */
  } while (x0 <= x1);
  while (y0-y1 < b) {
    /* too early stop of flat ellipses a=1 */
    put_pixel(x0-1, ++y0, clr); /* -> complete tip of ellipse */
    put_pixel(x0-1, --y1, clr);
  }
}


void draw_filled_ellipse (int x0, int y0, int x1, int y1, Uint32 clr) {
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; /* values of diameter */
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  long err = dx+dy+b1*a*a; /* error of 1.step */
  int prev_y0 = -1, prev_y1 = -1;
  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points... */
  if (y0 > y1) y0 = y1; /* ...exchange them */
  y0 += (b+1)/2; y1 = y0-b1; /* starting pixel */
  a *= 8*a; b1 = 8*b*b;
  do {
    int e2;
    if (y0 != prev_y0) { draw_hline(x0, y0, x1-x0+1, clr); prev_y0 = y0; }
    if (y1 != y0 && y1 != prev_y1) { draw_hline(x0, y1, x1-x0+1, clr); prev_y1 = y1; }
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } /* x step */
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  /* y step */
  } while (x0 <= x1);
  while (y0-y1 < b) {
    /* too early stop of flat ellipses a=1 */
    put_pixel(x0-1, ++y0, clr); /* -> complete tip of ellipse */
    put_pixel(x0-1, --y1, clr);
  }
}
#endif
