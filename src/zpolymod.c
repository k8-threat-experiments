/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "zpolymod.h"

#include <stdlib.h>
#include <stdio.h>


#ifdef ZPOLYMOD_LOGS
# define logf(...)  fprintf(stderr, __VA_ARGS__)
#endif

#ifndef logf
# define logf(...)  ((void)0)
#endif


#define K8MIN(__a,__b)  ({ typeof(__a) _a = (__a); typeof((__b))_b = (__b); _a < _b ? _a : _b; })
#define K8MAX(__a,__b)  ({ typeof(__a) _a = (__a); typeof((__b))_b = (__b); _a > _b ? _a : _b; })
#define K8ABS(__a)      ({ typeof(__a) _a = (__a); _a >= 0 ? _a : -(_a); })
#define K8SGN(__a)      ({ typeof(__a) _a = (__a); _a > 0 ? 1 : _a < 0 ? -1 : 0; })


polymod_hline_fn polymod_hline = NULL;
int polymod_screen_width = 320;
int polymod_screen_height = 240;


typedef struct {
  int next; /* -1: none */
  int dy;
  int x;
} poly_point_t;


static poly_point_t *ppool = NULL;
static int ppool_alloted = 0;
static int ppool_used = 0;

static int *pscans = NULL; /* index of the first point of each scan; -1: none */
static int pscans_size = 0;

static int min_y, max_y;
static int first_x0, first_y0; /* starting point */
static int first_x1, first_y1; /* starting segment */
static int last_x0, last_y0, last_x1, last_y1; /* last drawn non-horizontal segment */
static int dont_check_turn; /* don't check if this is 'turning point' */
static int seg_sx, seg_sy; /* previous vertex */
static int vertex_count; /* total number of added polygon vertex (not rasterized!) */
static int last_dy;


static void polymod_clear_pscans (void) {
  if (pscans_size < polymod_screen_height) {
    pscans = realloc(pscans, polymod_screen_height*sizeof(pscans[0]));
    if (pscans == NULL) {
      fprintf(stderr, "FATAL: out of memory in polymod!\n");
      abort();
    }
    pscans_size = polymod_screen_height;
  }
  for (int f = 0; f < polymod_screen_height; ++f) pscans[f] = -1;
}


void polymod_init (void) {
  polymod_clear_pscans();
  if (ppool_alloted < 8192) {
    ppool_alloted = 8192;
    ppool = realloc(ppool, ppool_alloted*sizeof(ppool[0]));
    if (ppool == NULL) {
      fprintf(stderr, "FATAL: out of memory in polymod!\n");
      abort();
    }
  }
  ppool_used = 0;
  vertex_count = 0;
}


void polymod_deinit (void) {
  if (pscans != NULL) free(pscans);
  pscans = NULL;
  pscans_size = 0;
  if (ppool != NULL) free(ppool);
  ppool = NULL;
  ppool_alloted = 0;
  ppool_used = 0;
  vertex_count = 0;
}


static inline int ppoint_alloc (int x, int dy, int next) {
  if (ppool_used+1 > ppool_alloted) {
    int newsz = ((ppool_alloted+1)|0x1fff)+1;
    poly_point_t *nn = realloc(ppool, newsz*sizeof(ppool[0]));
    if (nn == NULL) {
      fprintf(stderr, "FATAL: out of memory in polymod!\n");
      abort();
    }
    ppool = nn;
    ppool_alloted = newsz;
  }
  ppool[ppool_used].x = x;
  ppool[ppool_used].dy = dy;
  ppool[ppool_used].next = next;
  return ppool_used++;
}


static inline void ppoint_insert (int x, int y, int dy) {
  /* we don't want offscreen points */
  //logf(" pt: x=%2d; y=%2d\n", x, y);
  if (y >= 0 && y < polymod_screen_height) {
    int *p, n;
    /* there will be no huge number of points, so do linear lookup to keep points x-sorted */
    for (n = *(p = &pscans[y]); n != -1 && x > ppool[n].x; n = *(p = &ppool[n].next)) ;
    /* insert new point after p; n is the next point */
    *p = ppoint_alloc(x, dy, n);
  }
}


static inline int is_right (int x, int y, int x0, int y0, int x1, int y1) {
  return ((y-y0)*(x1-x0)-(x-x0)*(y1-y0) > 0);
}


/* vertical rasterization */
static void line_dda (int x0, int y0, int x1, int y1) {
  int dy, ody;
  logf("line_dda: (%d,%d)-(%d,%d)\n", x0, y0, x1, y1);
  /* if this is very first segment, remember it */
  if (vertex_count == 2) {
    first_x0 = x0;
    first_y0 = y0;
    first_x1 = x1;
    first_y1 = y1;
    logf(" *FIRST!\n");
  }
  ody = (last_y0 < last_y1 ? 1 : -1);
  if (y0 == y1) {
    /* horizontal line */
    logf(" *HORIZONTAL!\n");
    //!if (!dont_check_turn) ppoint_insert(x0, y0, (vertex_count == 0 ? 0 : ody));
    dont_check_turn = 1;
    last_x1 = x1;
    last_y1 = y1;
    return;
  }
  /* ppoint_insert() will do vertical clip, and we don't care about horizontal clip (which will be done in filling routine */
  min_y = K8MIN(min_y, K8MIN(y0, y1));
  max_y = K8MAX(max_y, K8MAX(y0, y1));
  /* should we check for 'turning point'? */
  last_dy = dy = (y0 < y1 ? 1 : -1);
  logf(" dont_check=%d; dy=%d; ody=%d; is_right=%d\n", dont_check_turn, dy, ody, is_right(x1, y1, last_x0, last_y0, last_x1, last_y1));
  if (!dont_check_turn && dy != ody && is_right(x1, y1, last_x0, last_y0, last_x1, last_y1)) {
    /* yes, this is 'turning point', add previous endpoint to list */
    //!ppoint_insert(last_x1, last_y1, ody);
  }
  dont_check_turn = 0; /* check next segment */
  /* remember this segment */
  last_x0 = x0;
  last_y0 = y0;
  last_x1 = x1;
  last_y1 = y1;
  /* now rasterize segment; don't include endpoint */
  /* dy>0: don't draw last point */
  /* dy<0: don't draw first point */
  if (x0 == x1) {
    /* straight vertical line */
    //!for (; y0 != y1; y0 += dy) ppoint_insert(x0, y0, dy);
    if (dy > 0) {
      for (; y0 < y1; ++y0) ppoint_insert(x0, y0, 1);
    } else {
      for (--y0; y0 >= y1; --y0) ppoint_insert(x0, y0, -1);
    }
  } else {
    /* sloped line */
    int sx = 1;
    int dx = K8ABS(x1-x0);
    int vy = K8ABS(y1-y0);
    int frac = 0;
    int xs = dx/vy;
    int fy = dx%vy;
    if (x0 > x1) { xs = -xs; sx = -1; }
    //logf(" dx=%d; vy=%d; xs=%d; sx=%d; fy=%d\n", dx, vy, xs, sx, fy);
    if (dy > 0) {
      while (y0 != y1) {
        ppoint_insert(x0, y0, dy);
        y0 += dy;
        x0 += xs;
        frac += fy;
        if (frac*2 >= vy) {
          x0 += sx;
          frac -= vy;
        }
      }
    } else {
      do {
        y0 += dy;
        x0 += xs;
        frac += fy;
        if (frac*2 >= vy) {
          x0 += sx;
          frac -= vy;
        }
        ppoint_insert(x0, y0, dy);
      } while (y0 != y1);
    }
  }
}


void polymod_start (void) {
  polymod_clear_pscans();
  ppool_used = 0;
  vertex_count = 0;
  min_y = polymod_screen_height;
  max_y = -1;
  last_x0 = last_y0 = last_x1 = last_y1 = 0;
  dont_check_turn = 0;
  last_dy = 0;
}


void polymod_add_vertex (int x, int y) {
  if (vertex_count > 0) {
    if (x == seg_sx && y == seg_sy) return;
    ++vertex_count;
    line_dda(seg_sx, seg_sy, x, y);
    /* start new segment */
    seg_sx = x;
    seg_sy = y;
  } else {
    first_x0 = seg_sx = x;
    first_y0 = seg_sy = y;
    ++vertex_count;
  }
}


void polymod_end (void) {
  if (vertex_count >= 3) {
    if (seg_sx != first_x0 || seg_sy != first_y0) line_dda(seg_sx, seg_sy, first_x0, first_y0);
    /* here ve have to check if we should add endpoint twice */
    if (last_x1 != first_x0 || last_y1 != first_y0) abort(); /* the thing that should not be */
    if (!dont_check_turn) {
      /* add first vertex if it is 'turning point' */
      int dy = (first_y0 < first_y1 ? 1 : -1);
      int ody = (last_y0 < last_y1 ? 1 : -1);
      if (dy != ody && is_right(first_x1, first_y1, last_x0, last_y0, last_x1, last_y1)) {
        //!ppoint_insert(last_x1, last_y1, ody);
      }
    }
    if (ppool[0].dy == 0) {
      logf("FHFIX!\n");
      ppool[0].dy = last_dy;
    }
  }
}


static void dump_scan (int y) {
  if (pscans[y] >= 0) {
    logf("y=%d:", y);
    for (int p = pscans[y]; p != -1; p = ppool[p].next) {
      logf(" (x=%d; dy=%d)", ppool[p].x, ppool[p].dy);
    }
    logf("\n");
  }
}


void polymod_fill (void) {
  if (vertex_count < 3) return;
  if (min_y < 0) min_y = 0;
  if (max_y > polymod_screen_height-1) max_y = polymod_screen_height-1;
  logf("--- polymod_fill ---\n");
  for (int y = min_y; y <= max_y; ++y) {
    dump_scan(y);
    int p = pscans[y];
    if (p >= 0) {
      int x = ppool[p].x;
      if (ppool[p].next == -1) {
        if (x >= 0 && x < polymod_screen_width) polymod_hline(x, y, 1);
      } else {
        int wind = ppool[p].dy;
        logf("y=%d; wind=%d; x=%d\n", y, wind, x);
        for (p = ppool[p].next; p != -1; p = ppool[p].next) {
          int ex = ppool[p].x;
          logf("  wind=%d; x=%d; ex=%d; dy=%d\n", wind, x, ex, ppool[p].dy);
          if (wind != 0) {
            int ee = (ex < polymod_screen_width-1 ? ex : polymod_screen_width-1);
            if (x < 0) x = 0;
            if (ee > x) polymod_hline(x, y, ee-x);
          }
          x = ex;
          wind += ppool[p].dy;
        }
      }
    }
  }
}
